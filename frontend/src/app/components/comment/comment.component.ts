import { Component, Input } from '@angular/core';
import { Comment } from 'src/app/models/comment/comment';
import { User } from 'src/app/models/user';
import { AuthenticationService } from 'src/app/services/auth.service';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { LikeService } from 'src/app/services/like.service';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { LikeType } from 'src/app/models/common/like-type';
import { Reaction } from 'src/app/models/reactions/reaction';
import { CommentService } from 'src/app/services/comment.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    get likeTypes() { return LikeType; }

    public isDeleted = false;
    public editCommentContainer = false;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService
    ) { }

    public reactionOnComment(reaction: LikeType) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.reactionOnComment(this.comment, userResp, reaction)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }
        debugger
        this.likeService
            .reactionOnComment(this.comment, this.currentUser, reaction)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public getReactionsByType(reactionType: LikeType, reactions: Reaction[]) {
        return this.likeService.getReactionsByType(reactionType, reactions);
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public toggleCommentContainer() {
        this.editCommentContainer = !this.editCommentContainer;
    }

    public updateComment() {
        const commentSubscription = this.commentService.updateComment(this.comment);

        commentSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respComment) => {
                this.editCommentContainer = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public deleteComment() {
        this.commentService
            .deleteComment(this.comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (respComment) => {
                    this.isDeleted = true;
                },
                (error) => this.snackBarService.showErrorMessage(error));
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
}
