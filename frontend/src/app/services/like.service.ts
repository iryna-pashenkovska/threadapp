import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { CommentService } from './comment.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { LikeType } from '../models/common/like-type';
import { Reaction } from '../models/reactions/reaction';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService, private commentService: CommentService) { }
    public reactionOnPost(post: Post, currentUser: User, userReaction: LikeType) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            likeType: userReaction,
            userId: currentUser.id
        };

        // update current array instantly
        innerPost.reactions = this.checkAndUpdateReactions(userReaction, innerPost.reactions, currentUser);

        return this.postService.reactionOnPost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = this.checkAndUpdateReactions(userReaction, innerPost.reactions, currentUser);

                return of(innerPost);
            })
        );
    }

    public checkAndUpdateReactions(reaction: LikeType, reactions: Reaction[], currentUser: User): Reaction[] {
        let hasReaction = reactions.some((x) => x.user.id === currentUser.id && x.likeType === reaction);
        let checkType = LikeType.Neutral;

        if (reaction === LikeType.Like) {
            checkType = LikeType.Dislike
        } else if (reaction === LikeType.Dislike) {
            checkType = LikeType.Like
        }

        let hasOppositeReaction = reactions.some((x) => x.user.id === currentUser.id && x.likeType === checkType);

        if (hasReaction) {  // remove placed like type
            return reactions = reactions.filter((x) => x.user.id !== currentUser.id)
        } else if (hasOppositeReaction) { // remove old like type and add new one
            return reactions = reactions.filter((x) => x.user.id !== currentUser.id).concat({ likeType: reaction, user: currentUser });
        } else { // add mew reaction
            return reactions = reactions.concat({ likeType: reaction, user: currentUser });
        }
    }

    public getReactionsByType(reactionType: LikeType, reactions: Reaction[]) {
        return reactions.filter(reaction => reaction.likeType === reactionType);
    }

    public reactionOnComment(comment: Comment, currentUser: User, userReaction: LikeType) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            likeType: LikeType.Like,
            userId: currentUser.id
        };

        // update current array instantly
        innerComment.reactions = this.checkAndUpdateReactions(userReaction, innerComment.reactions, currentUser);

        return this.commentService.reactionOnComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                innerComment.reactions = this.checkAndUpdateReactions(userReaction, innerComment.reactions, currentUser);

                return of(innerComment);
            })
        );
    }
}
