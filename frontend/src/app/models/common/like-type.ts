export enum LikeType {
    Dislike = -1,
    Neutral = 0,
    Like = 1
}