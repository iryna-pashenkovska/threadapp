import { User } from '../user';
import { LikeType } from '../common/like-type';

export interface Reaction {
    likeType: LikeType;
    user: User;
}
