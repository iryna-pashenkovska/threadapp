import { LikeType } from '../common/like-type';

export interface NewReaction {
    entityId: number;
    likeType: LikeType;
    userId: number;
}
