﻿using Thread_.NET.Common.DTO.User;
using Thread_.NET.Common.Enums;

namespace Thread_.NET.Common.DTO.Like
{
    public sealed class ReactionDTO
    {
        public ReactionType LikeType { get; set; }
        public UserDTO User { get; set; }
    }
}
