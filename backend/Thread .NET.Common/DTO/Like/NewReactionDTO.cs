﻿using Thread_.NET.Common.Enums;

namespace Thread_.NET.Common.DTO.Like
{
    public sealed class NewReactionDTO
    {
        public int EntityId { get; set; }
        public ReactionType LikeType { get; set; }
        public int UserId { get; set; }
    }
}
