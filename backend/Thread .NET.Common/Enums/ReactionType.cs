﻿namespace Thread_.NET.Common.Enums
{
    public enum ReactionType
    {
        Dislike = -1,
        Neutral = 0,
        Like = 1
    }
}
