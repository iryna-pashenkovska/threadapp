﻿using Thread_.NET.Common.Enums;

namespace Thread_.NET.DAL.Entities.Abstract
{
    public abstract class Reaction : BaseEntity
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public ReactionType LikeType { get; set; }
    }
}
