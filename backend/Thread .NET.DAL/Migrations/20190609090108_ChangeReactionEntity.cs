﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class ChangeReactionEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsLike",
                table: "PostReactions");

            migrationBuilder.DropColumn(
                name: "IsLike",
                table: "CommentReactions");

            migrationBuilder.AddColumn<int>(
                name: "LikeType",
                table: "PostReactions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LikeType",
                table: "CommentReactions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 3, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(4498), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(5635), 21 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 8, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9233), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9256), 18 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 9, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9339), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9346), 14 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 11, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9410), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9418), 8 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 9, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9467), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9475), 6 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 10, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9524), 1, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9531), 9 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 6, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9584), -1, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9592), 7 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 3, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9644), 1, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9652), 2 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 20, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9712), -1, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9720), 12 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 8, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9780), 1, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9792), 21 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 8, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9837), -1, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9845), 8 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 8, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9894), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9901), 7 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 2, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9950), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9958), 15 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt" },
                values: new object[] { 19, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(7), 1, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(14) });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 1, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(94), 1, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(105), 15 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 6, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(154), 1, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(165), 13 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 2, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(211), -1, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(218), 15 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 13, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(267), new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(275), 6 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 19, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(332), new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(339), 13 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 7, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(388), new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(400), 7 });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Id velit est quidem.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(1021), 5, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(2090) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Recusandae ullam corporis.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(3763), 12, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(3781) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Similique itaque est aliquam aut amet dicta.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(3925), 12, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(3936) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Atque et dolor quaerat quos voluptates eos saepe.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4072), 14, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4084) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Dolores voluptatibus repellat sunt.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4182), 11, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4189) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Et amet nobis.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4276), 18, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4287) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Eum porro non non ipsum porro.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4435), 11, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4446) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Maxime dolorem aut.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4586), 19, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4593) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Accusantium nesciunt voluptate et sint in et.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4744), 15, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4756) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Quibusdam est aperiam odio voluptate voluptatibus nam facere.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4873), 18, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4876) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Vel quia est fugiat fugiat cumque consequuntur labore fugiat.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4990), 4, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4997) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Atque numquam accusantium ad sint tenetur quae.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5084), 16, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5092) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Dolore quis et et et animi omnis et aliquam.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5239), 2, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5246) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Facilis hic nobis quasi dicta cumque corporis porro eveniet.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5337), 13, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5345) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Omnis modi vel.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5409), 15, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5416) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Quis non consequatur culpa sapiente aliquid voluptas aperiam harum.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5515), 10, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5522) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Nemo commodi cupiditate soluta sunt.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5594), 12, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5601) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Accusamus ut facilis ab ut magni maxime aut dolore.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5752), 5, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5764) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Possimus blanditiis cupiditate.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5832), 16, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5839) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Doloribus sit nemo.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5903), 2, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5911) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 267, DateTimeKind.Local).AddTicks(1696), "https://s3.amazonaws.com/uifaces/faces/twitter/darylws/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 267, DateTimeKind.Local).AddTicks(9418) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(589), "https://s3.amazonaws.com/uifaces/faces/twitter/sangdth/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(604) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(638), "https://s3.amazonaws.com/uifaces/faces/twitter/eugeneeweb/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(642) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(661), "https://s3.amazonaws.com/uifaces/faces/twitter/djsherman/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(668) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(683), "https://s3.amazonaws.com/uifaces/faces/twitter/scrapdnb/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(687) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(706), "https://s3.amazonaws.com/uifaces/faces/twitter/guiiipontes/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(710) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(729), "https://s3.amazonaws.com/uifaces/faces/twitter/tanveerrao/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(732) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(751), "https://s3.amazonaws.com/uifaces/faces/twitter/ecommerceil/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(755) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(770), "https://s3.amazonaws.com/uifaces/faces/twitter/clubb3rry/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(778) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(793), "https://s3.amazonaws.com/uifaces/faces/twitter/shojberg/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(797) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(815), "https://s3.amazonaws.com/uifaces/faces/twitter/mtolokonnikov/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(819) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(834), "https://s3.amazonaws.com/uifaces/faces/twitter/catarino/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(838) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(857), "https://s3.amazonaws.com/uifaces/faces/twitter/hasslunsford/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(861) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(880), "https://s3.amazonaws.com/uifaces/faces/twitter/a_harris88/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(883) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(902), "https://s3.amazonaws.com/uifaces/faces/twitter/sydlawrence/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(906) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(925), "https://s3.amazonaws.com/uifaces/faces/twitter/dreizle/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(929) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(944), "https://s3.amazonaws.com/uifaces/faces/twitter/constantx/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(951) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(966), "https://s3.amazonaws.com/uifaces/faces/twitter/ariil/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(970) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(989), "https://s3.amazonaws.com/uifaces/faces/twitter/andresenfredrik/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(993) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(1012), "https://s3.amazonaws.com/uifaces/faces/twitter/atanism/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(1016) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(2131), "https://picsum.photos/640/480/?image=530", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(2848) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3067), "https://picsum.photos/640/480/?image=63", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3079) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3173), "https://picsum.photos/640/480/?image=468", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3180) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3199), "https://picsum.photos/640/480/?image=128", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3203) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3222), "https://picsum.photos/640/480/?image=139", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3226) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3245), "https://picsum.photos/640/480/?image=683", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3248) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3267), "https://picsum.photos/640/480/?image=125", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3271) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3290), "https://picsum.photos/640/480/?image=309", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3294) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3313), "https://picsum.photos/640/480/?image=626", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3316) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3335), "https://picsum.photos/640/480/?image=349", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3339) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3358), "https://picsum.photos/640/480/?image=518", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3362) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3381), "https://picsum.photos/640/480/?image=164", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3384) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3399), "https://picsum.photos/640/480/?image=16", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3403) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3422), "https://picsum.photos/640/480/?image=53", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3426) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3498), "https://picsum.photos/640/480/?image=524", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3501) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3520), "https://picsum.photos/640/480/?image=458", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3524) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3543), "https://picsum.photos/640/480/?image=1015", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3547) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3566), "https://picsum.photos/640/480/?image=354", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3569) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3588), "https://picsum.photos/640/480/?image=783", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3592) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3611), "https://picsum.photos/640/480/?image=667", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3615) });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 578, DateTimeKind.Local).AddTicks(8159), -1, 12, new DateTime(2019, 6, 9, 12, 1, 7, 578, DateTimeKind.Local).AddTicks(9322), 6 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2437), -1, 11, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2463), 6 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2580), -1, 7, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2592), 14 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2675), 19, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2686), 19 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2769), -1, 20, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2777), 6 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2860), -1, 13, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2875), 18 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2966), -1, 16, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2977), 17 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3339), 12, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3362), 9 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3664), -1, 8, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3672), 18 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3758), -1, 2, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3770), 11 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3849), 17, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3860), 12 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3940), 17, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3951), 19 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4011), 1, 10, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4027), 7 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4098), 1, 4, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4106), 8 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4159), -1, 18, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4166), 20 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4242), 1, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4257), 13 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4332), -1, 4, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4344), 9 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4431), -1, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4442), 12 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4525), 17, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4540), 10 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4627), -1, 9, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4638), 21 });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Libero dolor unde sint unde qui similique quia in inventore.", new DateTime(2019, 6, 9, 12, 1, 7, 545, DateTimeKind.Local).AddTicks(8851), 21, new DateTime(2019, 6, 9, 12, 1, 7, 545, DateTimeKind.Local).AddTicks(9617) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "maiores", new DateTime(2019, 6, 9, 12, 1, 7, 546, DateTimeKind.Local).AddTicks(1562), 23, new DateTime(2019, 6, 9, 12, 1, 7, 546, DateTimeKind.Local).AddTicks(1581) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "voluptas", new DateTime(2019, 6, 9, 12, 1, 7, 546, DateTimeKind.Local).AddTicks(1656), 30, new DateTime(2019, 6, 9, 12, 1, 7, 546, DateTimeKind.Local).AddTicks(1664) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, @"Et veritatis qui quo cum.
Enim minus similique aut ab non in in temporibus eos.
Autem voluptas tenetur.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(266), 40, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(285) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "quo", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(368), 31, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(375) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Optio nulla reprehenderit. Vel non vero explicabo odit saepe. Dicta quia iusto rerum ratione. Natus mollitia velit totam voluptate eius porro sit autem. Saepe laborum velit reiciendis a sed quo officiis. Expedita eum voluptas.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(3924), 33, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(3940) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 13, "Culpa accusamus libero unde non est. Eos ea commodi iste quis fuga tempore ab. Ullam sapiente atque voluptates repudiandae ullam. Laborum natus id quos rerum quibusdam hic. Cumque qui id eos praesentium nihil. Consequatur et blanditiis.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4223), new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4230) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "sequi", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4264), 32, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4268) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Ut ut minima quae similique. Voluptatem inventore maiores molestiae velit. Nisi velit rerum at quo necessitatibus sed officia minus. Mollitia error qui architecto. Vel blanditiis reiciendis consectetur molestias neque totam id. Nihil voluptate dolorem vero qui est aspernatur.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4487), 29, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4491) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, @"Veniam corporis placeat occaecati.
Consectetur mollitia dolores autem quo deserunt.
Culpa itaque animi aut minus eligendi.
Officiis aut vel molestiae molestiae quisquam soluta dignissimos.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4770), 22, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4778) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "nulla", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4812), 36, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4816) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Tempora tempore inventore ut.
Rem magni impedit illum cum minima sed ab dicta.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4963), 35, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4974) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "fuga", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5080), 28, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5087) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Voluptas placeat excepturi sit modi accusamus et repudiandae veritatis. Ducimus repellendus veritatis harum distinctio repellat suscipit dolore quisquam. Quae ea labore aperiam doloremque dignissimos illum. A dolores consectetur minima et officiis exercitationem modi commodi.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5318), 30, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5321) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Numquam commodi animi corporis est reprehenderit consequatur fuga ipsa. Sequi impedit facere commodi molestias deleniti maiores dolor. Ipsa a non.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5465), 34, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5469) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Odio quidem aut suscipit aut. Veniam amet debitis et qui autem nihil. Sit sunt non consequuntur adipisci quia.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5571), 39, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5574) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Temporibus aut aut quis. Iusto nihil laudantium illo non maiores nesciunt quia. Aut totam quia et ab. Maiores fuga est fuga quo.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5722), 23, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5726) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, @"Quisquam facere esse quia explicabo fuga.
Ut ipsum repellat nemo sit quis.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5805), 30, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5809) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Vitae et possimus dicta fugiat non qui neque quia. Qui qui reprehenderit id. Facilis quia ut maxime sed quia.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5986), 30, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5994) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Illo hic et aliquam amet veniam.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(6062), 39, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(6065) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 9, 12, 1, 7, 314, DateTimeKind.Local).AddTicks(1895), "Joaquin70@gmail.com", "ky9kx3wOsL6x8EndxkxU/gEcvxpMV6BVAQR8s93WGZ8=", "05g1L/iALYmDEXVbgWW9425sceGegaAdkvvQVrAPyb8=", new DateTime(2019, 6, 9, 12, 1, 7, 314, DateTimeKind.Local).AddTicks(2703), "Maureen32" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2019, 6, 9, 12, 1, 7, 322, DateTimeKind.Local).AddTicks(6843), "Delfina.Purdy34@yahoo.com", "JgG11NZ80BMdWXJBTGV4reS/48eX9fV9QD3NadJU3Y4=", "vEi/Ha6oYsFxQu5XY8MT4CF9TMdm3JYLZO0MgIe7jE8=", new DateTime(2019, 6, 9, 12, 1, 7, 322, DateTimeKind.Local).AddTicks(6877), "Wade1" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2019, 6, 9, 12, 1, 7, 331, DateTimeKind.Local).AddTicks(1429), "Tod_Hilpert@gmail.com", "BdG3mUmIysdcpEW0jbSdvVuUZ7FAD5e2TpQJxnxwBhU=", "bc1la/1ktIoSGjYr5y9EmDzOJj58PzWmplqZLn+EFwQ=", new DateTime(2019, 6, 9, 12, 1, 7, 331, DateTimeKind.Local).AddTicks(1441), "Roscoe.Ziemann" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2019, 6, 9, 12, 1, 7, 342, DateTimeKind.Local).AddTicks(1282), "Demetrius17@hotmail.com", "1bPNHDVcgoAHPT+Fb7iZXC8UlrncRQn9HBQLPD6r/p8=", "X6EsvN7kF+yGPGDmLJ2zHtlyxJLHlGpV++A1ZiP91fM=", new DateTime(2019, 6, 9, 12, 1, 7, 342, DateTimeKind.Local).AddTicks(1316), "Jaleel66" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 9, 12, 1, 7, 355, DateTimeKind.Local).AddTicks(3938), "Neva.Koss@yahoo.com", "VllqrWUVLOHWUxaaObs0vbb9w3iFcNSVZYZi7qHfN7g=", "Xy7DGYCaZMd78Fd+LxXkeJg3gGOz4Qd7OWTqy2Fdf8Q=", new DateTime(2019, 6, 9, 12, 1, 7, 355, DateTimeKind.Local).AddTicks(3968), "Judson92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2019, 6, 9, 12, 1, 7, 364, DateTimeKind.Local).AddTicks(1714), "Brooke.Quigley@hotmail.com", "jT+5XHBUQXAWLOqzar4CHIkYtaDCI8cwxq7EeW3pehs=", "UrrS8vbDqk6UnQ5oXeB6M7OW3xQyY289pTGxfhakF+k=", new DateTime(2019, 6, 9, 12, 1, 7, 364, DateTimeKind.Local).AddTicks(1756), "Telly.Hackett" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 9, 12, 1, 7, 376, DateTimeKind.Local).AddTicks(9507), "Geo98@gmail.com", "G0kPxjRDhnvqLvPrgc477BRlCgHFNPPSB+tMrgG35f0=", "iXzBPHu1JRM7vQ9qtv0QXAy/Nu7Ezo3qtuy/zcNsIe8=", new DateTime(2019, 6, 9, 12, 1, 7, 376, DateTimeKind.Local).AddTicks(9556), "Kristofer.Schaefer" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 391, DateTimeKind.Local).AddTicks(8066), "Fredrick_Gottlieb71@gmail.com", "kmcWD7KBY6Iid8PLFrtlThDTJAWyniq8U5ZLFYMiaLo=", "Ih/rD2Giiosd7c1l1X/Cd7t73WObquqpA6VWPLMt+WE=", new DateTime(2019, 6, 9, 12, 1, 7, 391, DateTimeKind.Local).AddTicks(8115), "Stacey51" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2019, 6, 9, 12, 1, 7, 402, DateTimeKind.Local).AddTicks(3547), "Rebeca_Nikolaus87@yahoo.com", "dWTzWJBDFZA1YT/GPSva69M6Jiy5K0X+4Zi+fONql5w=", "JsLICz8r9/obefiMYXvXqCuJHAnRR3CRFaxpV0IrDlY=", new DateTime(2019, 6, 9, 12, 1, 7, 402, DateTimeKind.Local).AddTicks(3581), "Ronny81" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2019, 6, 9, 12, 1, 7, 411, DateTimeKind.Local).AddTicks(2146), "Melvina_White15@yahoo.com", "zuvrH7Cfy0lnCJOpWtDgwb81lo3HhnH8zYk5l6I2oQE=", "DNl0ooLJAwWLAJX3NPbLIGTy8K7dYqdRSrzQh8C0N0U=", new DateTime(2019, 6, 9, 12, 1, 7, 411, DateTimeKind.Local).AddTicks(2180), "Ariel.Krajcik" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2019, 6, 9, 12, 1, 7, 419, DateTimeKind.Local).AddTicks(8276), "Darian_Quigley@yahoo.com", "g8iN35AuyFLiBCBgB6Fyr7AB4BLhuO3WHmLu1Mwt7r4=", "Uxl4KoemTeIY10sgHugVTpz0bECFHdS1f60O50RFv94=", new DateTime(2019, 6, 9, 12, 1, 7, 419, DateTimeKind.Local).AddTicks(8318), "Soledad.Price" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2019, 6, 9, 12, 1, 7, 428, DateTimeKind.Local).AddTicks(4256), "Frederique81@yahoo.com", "qWQ8AGCyIrZATHjYBhq+V+GaIVBSs0nF5Q1pntZksWw=", "SfOB801X6p8x7m4Zzy3D0hdp6N/mnail8DPKeK/5wlQ=", new DateTime(2019, 6, 9, 12, 1, 7, 428, DateTimeKind.Local).AddTicks(4282), "Cornell49" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2019, 6, 9, 12, 1, 7, 437, DateTimeKind.Local).AddTicks(9633), "Jordon_Armstrong@gmail.com", "tBXA9OKuIfZVvfggsWWuUxvuKexf/TO/uC4q2EZc4rw=", "QSW16kNtVz/HcL2wqEbBw14yEh7nJutTctiuQ+Z1luk=", new DateTime(2019, 6, 9, 12, 1, 7, 437, DateTimeKind.Local).AddTicks(9678), "Orion.Harvey" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2019, 6, 9, 12, 1, 7, 446, DateTimeKind.Local).AddTicks(3267), "Alexandrine_Waelchi73@hotmail.com", "rNC0UpCWKjkrUNnwdCWTFxYYkoYNp/+HkQNQuxHQ20A=", "r2DRU2zD4DG6FdoWkxflGo4hXKgj2rmFJKEAQtlHKU0=", new DateTime(2019, 6, 9, 12, 1, 7, 446, DateTimeKind.Local).AddTicks(3305), "Delilah_Kris70" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2019, 6, 9, 12, 1, 7, 459, DateTimeKind.Local).AddTicks(3835), "Penelope.Nienow@yahoo.com", "KiOd6tpx0iQ22viH36Q9HU4eg7PYTYZXHuC9FyBaDL8=", "w2y0gCNnu2IDexEWdjtZus8raptpH9WQCbuqDwEEx2o=", new DateTime(2019, 6, 9, 12, 1, 7, 459, DateTimeKind.Local).AddTicks(3884), "Madonna_Lindgren" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2019, 6, 9, 12, 1, 7, 471, DateTimeKind.Local).AddTicks(8874), "Ara_Willms21@gmail.com", "swVrE0lgSNtlB5b5vzXFDWmWB1dVUji8ZHdxTc8lzxI=", "tihI/4B2kLiR7qT1HilyS24lXIbkjTIVk/kArC3wf/Y=", new DateTime(2019, 6, 9, 12, 1, 7, 471, DateTimeKind.Local).AddTicks(8942), "Gene_Schmeler59" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2019, 6, 9, 12, 1, 7, 487, DateTimeKind.Local).AddTicks(7229), "Charley_Gerlach32@gmail.com", "FiAdy7zCoUyNakFlxvS4gMcJcGGiCDfMvAyNUelbSlM=", "vxNPazXQx2GppfVhHclEH761FUZgPxyOI+3mgk/2W+E=", new DateTime(2019, 6, 9, 12, 1, 7, 487, DateTimeKind.Local).AddTicks(7278), "Muriel.Pagac97" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2019, 6, 9, 12, 1, 7, 499, DateTimeKind.Local).AddTicks(4185), "Kimberly.Flatley@hotmail.com", "2LnsQrZUUcEhPPCqJbbkW/9Po1cWLmTnYwvqwmDvGUU=", "P1VYEjVhwgT2hApAuwyCBQv2e7nRAwkuaweYSrbHYp4=", new DateTime(2019, 6, 9, 12, 1, 7, 499, DateTimeKind.Local).AddTicks(4222), "Marjorie.Mills" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2019, 6, 9, 12, 1, 7, 512, DateTimeKind.Local).AddTicks(886), "Judah.Hackett@hotmail.com", "JsyqJEwYtEoK8NdgY1AzLYAJSEQUSUXOb2DmkgNxviY=", "j2+gElzhfaQQJzAnb4CRZdOR28Bbvxoexlo/u2rQ9h8=", new DateTime(2019, 6, 9, 12, 1, 7, 512, DateTimeKind.Local).AddTicks(935), "Morgan.Jones52" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 9, 12, 1, 7, 524, DateTimeKind.Local).AddTicks(6907), "Coy29@yahoo.com", "XeYRc0vSmE5lQQjCTd6E4JDR6/sUhapz3Q249gUQRYY=", "n2CgoxIyxZ2AvnRWmMK2jndVtmtTNZSTxk1jcVoS7Jg=", new DateTime(2019, 6, 9, 12, 1, 7, 524, DateTimeKind.Local).AddTicks(6949), "Lucienne_Jerde" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 532, DateTimeKind.Local).AddTicks(8721), "c+fl6yzl+ooS+Vn/sEP3leUPy60WaLJsMP0nmO+B/bY=", "MxFf4jpD6JI1XIG7ZElNeZz3YpeOEw+NENQ5ud5q67I=", new DateTime(2019, 6, 9, 12, 1, 7, 532, DateTimeKind.Local).AddTicks(8721) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LikeType",
                table: "PostReactions");

            migrationBuilder.DropColumn(
                name: "LikeType",
                table: "CommentReactions");

            migrationBuilder.AddColumn<bool>(
                name: "IsLike",
                table: "PostReactions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsLike",
                table: "CommentReactions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[] { 8, new DateTime(2019, 4, 10, 13, 42, 27, 539, DateTimeKind.Local).AddTicks(9429), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(424), 8 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 12, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1237), new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1250), 1 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[] { 20, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1291), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1296), 11 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[] { 17, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1326), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1331), 16 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[] { 7, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1356), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1361), 13 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 16, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1385), new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1390), 13 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 5, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1414), new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1418), 16 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 12, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1443), new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1448), 4 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[] { 5, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1476), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1481), 20 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[] { 7, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1505), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1510), 15 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[] { 16, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1534), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1538), 1 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 18, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1563), new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1567), 9 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 1, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1614), new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1620), 10 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt" },
                values: new object[] { 20, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1646), new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1651) });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 2, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1678), new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1682), 12 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[] { 7, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1707), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1711), 18 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 1, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1735), new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1740), 18 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 4, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1763), new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1768), 9 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[] { 4, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1791), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1796), 15 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 2, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1825), new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1830), 17 });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Odio earum expedita eos magnam voluptas quia aut ipsum.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(1617), 3, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(2611) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Illum et neque facilis.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3483), 14, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3498) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Ullam magnam quis nemo eum dolor quaerat eum.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3603), 10, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3610) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Fugiat delectus aspernatur soluta sit sed provident nostrum et.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3691), 19, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3698) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Modi in officiis itaque et et quas.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3759), 8, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3766) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Reiciendis quia perspiciatis et quidem.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3830), 8, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3836) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Et accusamus ea voluptatem harum dolorem eaque eos ducimus fuga.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3906), 10, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3913) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Rerum iure libero voluptatem.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3983), 11, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3990) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Possimus dolor fugiat modi voluptatum rem doloribus necessitatibus dolores.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4056), 19, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4063) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Recusandae molestias eius ipsa sit.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4120), 19, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4126) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Ut commodi sed aut ratione saepe sed doloremque.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4188), 18, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4194) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Et assumenda sint labore voluptatem eaque dolor nulla neque quia.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4281), 1, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4289) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Quisquam vero unde et qui.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4350), 10, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4356) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Repellendus quibusdam earum qui.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4420), 20, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4427) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Culpa enim a.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4479), 6, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4486) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Asperiores corrupti quam enim.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4538), 1, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4544) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Inventore dolor provident.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4593), 8, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4599) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Nesciunt nostrum ut unde corporis rem reprehenderit libero quia ut.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4665), 17, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4672) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Sunt odit voluptatem saepe eius.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4725), 15, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4732) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Est consequatur consequuntur eum.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4880), 7, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4888) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 288, DateTimeKind.Local).AddTicks(1098), "https://s3.amazonaws.com/uifaces/faces/twitter/belyaev_rs/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 288, DateTimeKind.Local).AddTicks(9343) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(576), "https://s3.amazonaws.com/uifaces/faces/twitter/bpartridge/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(590) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(630), "https://s3.amazonaws.com/uifaces/faces/twitter/martip07/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(636) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(668), "https://s3.amazonaws.com/uifaces/faces/twitter/ricburton/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(674) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(697), "https://s3.amazonaws.com/uifaces/faces/twitter/sreejithexp/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(702) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(725), "https://s3.amazonaws.com/uifaces/faces/twitter/longlivemyword/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(730) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(754), "https://s3.amazonaws.com/uifaces/faces/twitter/ajaxy_ru/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(759) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(782), "https://s3.amazonaws.com/uifaces/faces/twitter/kalmerrautam/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(787) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(817), "https://s3.amazonaws.com/uifaces/faces/twitter/gonzalorobaina/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(822) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(844), "https://s3.amazonaws.com/uifaces/faces/twitter/vitor376/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(850) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(872), "https://s3.amazonaws.com/uifaces/faces/twitter/baumannzone/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(877) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(900), "https://s3.amazonaws.com/uifaces/faces/twitter/andreas_pr/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(905) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(928), "https://s3.amazonaws.com/uifaces/faces/twitter/rtgibbons/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(933) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(963), "https://s3.amazonaws.com/uifaces/faces/twitter/timothycd/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(968) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(991), "https://s3.amazonaws.com/uifaces/faces/twitter/adobi/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(996) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1231), "https://s3.amazonaws.com/uifaces/faces/twitter/moynihan/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1240) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1282), "https://s3.amazonaws.com/uifaces/faces/twitter/weavermedia/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1288) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1312), "https://s3.amazonaws.com/uifaces/faces/twitter/nvkznemo/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1317) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1340), "https://s3.amazonaws.com/uifaces/faces/twitter/exentrich/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1345) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1369), "https://s3.amazonaws.com/uifaces/faces/twitter/cbracco/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1374) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(906), "https://picsum.photos/640/480/?image=392", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(1980) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2289), "https://picsum.photos/640/480/?image=425", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2301) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2335), "https://picsum.photos/640/480/?image=784", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2340) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2365), "https://picsum.photos/640/480/?image=758", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2370) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2392), "https://picsum.photos/640/480/?image=726", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2397) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2419), "https://picsum.photos/640/480/?image=866", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2423) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2445), "https://picsum.photos/640/480/?image=557", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2450) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2471), "https://picsum.photos/640/480/?image=263", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2475) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2498), "https://picsum.photos/640/480/?image=60", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2503) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2593), "https://picsum.photos/640/480/?image=222", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2599) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2625), "https://picsum.photos/640/480/?image=201", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2630) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2651), "https://picsum.photos/640/480/?image=217", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2656) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2678), "https://picsum.photos/640/480/?image=730", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2682) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2705), "https://picsum.photos/640/480/?image=1061", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2709) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2731), "https://picsum.photos/640/480/?image=52", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2736) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2758), "https://picsum.photos/640/480/?image=467", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2762) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2784), "https://picsum.photos/640/480/?image=385", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2788) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2810), "https://picsum.photos/640/480/?image=19", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2814) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2836), "https://picsum.photos/640/480/?image=341", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2840) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2862), "https://picsum.photos/640/480/?image=685", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2866) });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(2442), true, 1, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(3481), 4 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4462), true, 1, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4478), 4 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4526), true, 16, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4531), 17 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4560), true, 14, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4565), 16 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4592), 11, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4597), 19 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4624), 17, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4628), 11 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4655), true, 19, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4660), 12 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4685), 18, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4690), 17 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4715), true, 13, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4720), 15 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4744), 19, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4749), 10 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4773), true, 10, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4778), 4 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4812), true, 10, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4818), 20 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4843), true, 8, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4848), 15 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4873), 6, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4878), 6 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4904), true, 4, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4909), 1 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4933), new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4938), 10 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4962), 17, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4967), 18 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4991), true, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4996), 21 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(5021), true, 8, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(5026), 13 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(5049), true, 11, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(5054), 20 });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "molestiae", new DateTime(2019, 4, 10, 13, 42, 27, 514, DateTimeKind.Local).AddTicks(4538), 29, new DateTime(2019, 4, 10, 13, 42, 27, 514, DateTimeKind.Local).AddTicks(5593) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, @"Nisi quia sunt sapiente sapiente quia maiores iure.
Non rerum pariatur rem id autem.
Accusantium iure odio et possimus.
Rerum illum ut unde sed qui aperiam corrupti.
Et enim repellendus dolor inventore inventore unde cupiditate tempore.
Aut possimus temporibus pariatur at voluptas dolor minima.", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(5149), 32, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(5171) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Sapiente itaque fuga sed ad.", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7346), 40, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7361) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "sed", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7457), 28, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7464) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, @"Est nihil id corporis voluptatum eos voluptas.
Tenetur delectus pariatur ut.", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7654), 23, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7663) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Rem aspernatur excepturi et.", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7754), 27, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7761) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 19, @"Iste aut et qui qui consequatur ipsa aut minima.
Ea perspiciatis aliquam.
Voluptate occaecati id voluptatibus expedita nobis nemo officiis harum quisquam.
Aut pariatur ut molestiae ut.", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7926), new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7933) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "sint", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7985), 39, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7992) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "vitae", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(8036), 31, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(8042) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Cumque reiciendis et numquam sit aut explicabo sunt et. Quo deleniti nobis voluptatum eveniet dolor sunt a aut vel. Labore quos voluptas.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(1677), 35, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(1693) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Voluptatum dolores officiis qui aperiam recusandae quod minus.
Libero dolores sit eligendi reiciendis delectus vel eaque.
Ad aliquam corporis recusandae.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(1890), 25, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(1899) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Corrupti corporis voluptatum sit dignissimos est. Iure ea sapiente laboriosam et perspiciatis nesciunt quae et quis. Quae voluptatem voluptatem non hic cumque voluptas vel. Ut perspiciatis exercitationem. Ex quia totam magnam et amet corrupti est est. Officia natus quam optio culpa minus aspernatur quaerat accusantium.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2146), 39, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2154) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, @"Sapiente ex omnis quam.
Id sunt voluptas.
Explicabo voluptatibus dignissimos.
Et laborum quisquam.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2261), 37, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2268) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Tempore delectus non consequatur sit est voluptatem.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2455), 22, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2464) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Dolorum optio ut nostrum beatae velit et.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2541), 33, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2547) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "maxime", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2591), 29, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2597) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, @"Consectetur est nemo fugit ea exercitationem a non.
Dolorem assumenda ut quam qui corrupti voluptas qui error commodi.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2709), 35, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2716) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, @"Minus ex odio aliquid molestiae.
Sed nostrum quae doloribus eum odio quia.
Ipsum quia quod id exercitationem.
Rerum fugiat quia esse impedit.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2846), 29, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2853) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Pariatur in aut alias eum alias. Est id voluptatum fuga deleniti amet et facere quae. Totam reprehenderit ipsa. Quasi voluptatem veniam.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2984), 34, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2991) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "voluptatem", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(3034), 33, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(3041) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 4, 10, 13, 42, 27, 354, DateTimeKind.Local).AddTicks(9279), "Eleanore78@hotmail.com", "JwhgJRWNi9KZ6HvsVC4o3ATXPKrIl6NpFs14Ywn6XcQ=", "1ZKXAVI9IEeT90K/cc4u5/9FM9h5NSvHrOT3p8VVgXk=", new DateTime(2019, 4, 10, 13, 42, 27, 355, DateTimeKind.Local).AddTicks(557), "Reanna.Berge" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2019, 4, 10, 13, 42, 27, 362, DateTimeKind.Local).AddTicks(6332), "Noel37@gmail.com", "Ov/w2Ir6JjrZM+lrZWDJZQdRTyrjeX3kkEV1sNq5DLM=", "81E5KuSfD6Wy6OFVRVrzJi2uxOmPDeOHy31ZANgLikw=", new DateTime(2019, 4, 10, 13, 42, 27, 362, DateTimeKind.Local).AddTicks(6356), "Trevion.Konopelski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2019, 4, 10, 13, 42, 27, 370, DateTimeKind.Local).AddTicks(1374), "Tyree.Hettinger5@hotmail.com", "IFV9xQIA/JkXs2hSSZbxmkQc94MfDE6An6s8rISbMYo=", "lt+NT9Cw+2mitIPEmnHcp4xH3eoNtuwBukz7l2yl2Rg=", new DateTime(2019, 4, 10, 13, 42, 27, 370, DateTimeKind.Local).AddTicks(1394), "Raoul_Armstrong" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2019, 4, 10, 13, 42, 27, 377, DateTimeKind.Local).AddTicks(6273), "Landen82@gmail.com", "pel2LSd08C+t2mEhX1uBO7rVCWTaKiAjkqj43O1ytxo=", "W/OnJ8Ygem16yrXzV3ijvpYdye+/hgN2MeIiz/j9spM=", new DateTime(2019, 4, 10, 13, 42, 27, 377, DateTimeKind.Local).AddTicks(6291), "Tamara_Heaney59" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2019, 4, 10, 13, 42, 27, 385, DateTimeKind.Local).AddTicks(1240), "Ashlee_Walker@gmail.com", "Ty1WmblkkeeehqZtK993rkptymgJ4HHmP3FtlQwggTQ=", "xmkPKualMGfdZNrMieig0XspCqfUdXIsiM9n+hH0BNM=", new DateTime(2019, 4, 10, 13, 42, 27, 385, DateTimeKind.Local).AddTicks(1258), "April.Baumbach63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2019, 4, 10, 13, 42, 27, 392, DateTimeKind.Local).AddTicks(4481), "Vern_Yost17@hotmail.com", "UrtG6PTWOuZN01Y0wJW2RXzWQztXetNZKLMXY2kPapU=", "nI8AsOHQ9rdpquComXIuzdUI83EJA5Ebtk53tHp8okM=", new DateTime(2019, 4, 10, 13, 42, 27, 392, DateTimeKind.Local).AddTicks(4494), "Jessy_Trantow70" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2019, 4, 10, 13, 42, 27, 400, DateTimeKind.Local).AddTicks(1588), "Octavia.Breitenberg15@gmail.com", "JEPtNmc+xcRh6dRMz0XpW3YjPfo70dx6ghqpRbR7qZ8=", "QkAvLRdlO782y7r9/FQ5BeOVYPCV2OixVUgbJ0x0kZQ=", new DateTime(2019, 4, 10, 13, 42, 27, 400, DateTimeKind.Local).AddTicks(1618), "Brionna_Huel" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 407, DateTimeKind.Local).AddTicks(5516), "Jaida.Robel@yahoo.com", "2sM5eXjzXM4rit4n0XznRgPXWrMUrkM2NEYIt8wWdM0=", "uZISArgxWHVs5sNqC3KY/zSN2GC6+cQ4PbSZXd2tY/w=", new DateTime(2019, 4, 10, 13, 42, 27, 407, DateTimeKind.Local).AddTicks(5528), "Idella67" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 4, 10, 13, 42, 27, 415, DateTimeKind.Local).AddTicks(286), "Merle3@hotmail.com", "vPqcCQvAy3m0ApPP9wTVPS+S6M+kffrzhI0KKY5byUE=", "73Y6XFrs5GNfbX3gEPnUriXG+09YbIzyLCyKeTM4d/4=", new DateTime(2019, 4, 10, 13, 42, 27, 415, DateTimeKind.Local).AddTicks(305), "Francesca_McClure" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2019, 4, 10, 13, 42, 27, 422, DateTimeKind.Local).AddTicks(5128), "Destin87@gmail.com", "3OzL76X0wfbkPwt7JQPH+/ebvc9xfBO3bcFQunQ0v4c=", "E4foLxFiXVPkgaoQ+Tr6Ge0g6OSnwWDa3eNX3Y0ExT4=", new DateTime(2019, 4, 10, 13, 42, 27, 422, DateTimeKind.Local).AddTicks(5147), "Ben.Legros80" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 4, 10, 13, 42, 27, 430, DateTimeKind.Local).AddTicks(2390), "Finn84@gmail.com", "izsFaOQN3vHcHSvx8GGNmww7cWdIFOMBLISdizTYcw8=", "QwJmN2g2pIq3vU+epDXifJpLcfBOM84mE8O97MdJkzg=", new DateTime(2019, 4, 10, 13, 42, 27, 430, DateTimeKind.Local).AddTicks(2408), "Claudie_Von90" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2019, 4, 10, 13, 42, 27, 437, DateTimeKind.Local).AddTicks(7089), "Brady24@yahoo.com", "fvm+iVG/i1teAwJu6gjd99cM3VzEf33Q4lZmpYa3gtA=", "qwNd4zHDb4xaQdyEy9lNs/c4y5rsfGTbauvyVwf8COc=", new DateTime(2019, 4, 10, 13, 42, 27, 437, DateTimeKind.Local).AddTicks(7102), "Bertha_Kuhic" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2019, 4, 10, 13, 42, 27, 445, DateTimeKind.Local).AddTicks(471), "Amira84@yahoo.com", "OO/o8dRJNQHE/Cy5SsMuUoV59W4CfSqsYA1JYbnBbKA=", "BcFPRW2kgTsEIJrwMiZMtEJGYYttXjV8hE2B6jCKgSQ=", new DateTime(2019, 4, 10, 13, 42, 27, 445, DateTimeKind.Local).AddTicks(482), "Roxane.Bechtelar" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2019, 4, 10, 13, 42, 27, 452, DateTimeKind.Local).AddTicks(5168), "Reagan_Gottlieb96@gmail.com", "8/CAtMOKHMOIgZYZyjfPyFgxlGUPh2TRexcrWcVuKR0=", "N4HTAz9evQh9HSBh4Ui10gobECk9Re8EzmJd04m9PKc=", new DateTime(2019, 4, 10, 13, 42, 27, 452, DateTimeKind.Local).AddTicks(5186), "Willow84" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2019, 4, 10, 13, 42, 27, 460, DateTimeKind.Local).AddTicks(40), "Dane_Toy25@hotmail.com", "5ksghg+QRIoR4HCwib/D71e6GWEgIzgvHUy+/jIU8u4=", "YHCWI8ogFKXeVnbDjONhQnBcwOv9KTCv65i5aPHgxy4=", new DateTime(2019, 4, 10, 13, 42, 27, 460, DateTimeKind.Local).AddTicks(52), "Lyric_Powlowski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2019, 4, 10, 13, 42, 27, 467, DateTimeKind.Local).AddTicks(7399), "Clyde.Kunze@gmail.com", "jN562rlj3Y4M9JiXhpaq22V9r5A0MCACnb8p5a03L24=", "PDjMLphnPnJG54zhHoLXWCW2Jr4kTqmaumBZuqRp0GM=", new DateTime(2019, 4, 10, 13, 42, 27, 467, DateTimeKind.Local).AddTicks(7433), "Kevon45" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2019, 4, 10, 13, 42, 27, 475, DateTimeKind.Local).AddTicks(6227), "Chanel.Champlin68@hotmail.com", "AH5M8V4Q25IYweo/q/iVqQQ4h/xed8dxtZE0aeS5cWM=", "73owabAASkyG0Dvg2TnKlvWRX0FzS0ur49Ur3J5H3qM=", new DateTime(2019, 4, 10, 13, 42, 27, 475, DateTimeKind.Local).AddTicks(6272), "Jewell_Schmitt" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 4, 10, 13, 42, 27, 483, DateTimeKind.Local).AddTicks(1289), "Watson.Bahringer@yahoo.com", "Q62jzI0XYrLxW7Vfi/yI+uS4YjSlRcUbQZxMQGfc8C0=", "vSbXm9StFleAMjE1o2oRvdW3lJHYt7hkHrsLCa7Dg4U=", new DateTime(2019, 4, 10, 13, 42, 27, 483, DateTimeKind.Local).AddTicks(1302), "Jazmin98" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2019, 4, 10, 13, 42, 27, 490, DateTimeKind.Local).AddTicks(5812), "Aditya.Kerluke@yahoo.com", "7D2UgGVDo4CTiK0CFT/Cd2Kryi7caZwemc5a7f9Xus8=", "zUsIM0NUWcyfmuUZWfbZD/ip+Wl+JZe/hLUSru5/rSM=", new DateTime(2019, 4, 10, 13, 42, 27, 490, DateTimeKind.Local).AddTicks(5824), "Josefa_Brakus" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2019, 4, 10, 13, 42, 27, 498, DateTimeKind.Local).AddTicks(265), "Stewart.Kiehn@hotmail.com", "huLmL4GntVXBmnUFICkmowedAskr5IoAk5wW5424zqQ=", "AmgoIHeRfU91KKABVopqN9E6rEyTLpgLW9RqszTP2zc=", new DateTime(2019, 4, 10, 13, 42, 27, 498, DateTimeKind.Local).AddTicks(276), "Margaretta_Schaden" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 505, DateTimeKind.Local).AddTicks(3866), "C+XvvutUOnUo6U86oKynPexTPQkkhKfd4pYAf+LzYPE=", "wBQhcRU+Nh0iNrhTwxtzwOXjMhOxlLTx5BkVJ3h6sB4=", new DateTime(2019, 4, 10, 13, 42, 27, 505, DateTimeKind.Local).AddTicks(3866) });
        }
    }
}
