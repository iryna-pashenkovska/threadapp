﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddSoftDeleteAction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Posts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Comments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 13, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(4071), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(5170), 16 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 17, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6321), -1, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6336), 6 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 7, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6393), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6397), 11 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 18, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6427), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6431), 21 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 15, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6461), -1, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6465), 4 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 18, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6491), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6495), 13 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 5, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6521), 0, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6529), 4 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 10, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6555), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6559), 18 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 18, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6646), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6650), 8 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 12, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6684), 0, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6687), 12 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 5, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6725), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6729), 7 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 2, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6763), -1, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6770), 10 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 10, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6797), -1, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6801), 8 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 20, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6838), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6842), 5 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt" },
                values: new object[] { 11, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6869), 0, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6872) });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 5, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6899), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6903), 11 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 17, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6933), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6937), 17 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6963), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6967), 21 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6997), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(7001), 12 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 19, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(7031), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(7035), 1 });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Voluptatem nemo vitae explicabo labore tempora temporibus labore consequatur autem.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(1651), 3, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(2349) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Natus sed rem corporis.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3135), 8, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3176) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Officiis deserunt et ipsa culpa.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3252), 3, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3255) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Aliquid vel non quia eos accusantium.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3353), 8, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3357) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Quia totam totam provident quia quia.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3406), 12, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3410) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Et id ut.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3448), 20, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3452) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Nostrum et et tempore aut eaque sint.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3497), 3, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3501) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 4, "Necessitatibus doloribus ut nostrum minima.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3542), new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3546) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Beatae dolor aut rerum omnis unde ut veritatis atque.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3599), 12, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3603) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Non tenetur ea ut rerum itaque.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3690), 20, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3693) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Ex est id ex quia et sit.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3735), 6, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3742) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Dolorum illum voluptatem voluptatem autem illum voluptate ratione sint.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3791), 15, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3795) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Alias nostrum minus eum itaque officia commodi.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3841), 7, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3844) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Rerum amet quisquam sunt.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3882), 11, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3886) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Sed recusandae necessitatibus numquam qui tenetur natus.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3961), 13, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3969) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Vitae deleniti temporibus dignissimos similique illum et.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4014), 7, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4018) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Ut dolores maxime consequatur et dignissimos qui.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4060), 20, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4067) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Eius et officiis unde in totam at.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4109), 6, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4112) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Quia qui ullam aut minima.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4150), 12, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4154) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Autem quis sint ut necessitatibus repudiandae aliquam dolorum consectetur.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4203), 8, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4207) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 481, DateTimeKind.Local).AddTicks(109), "https://s3.amazonaws.com/uifaces/faces/twitter/moynihan/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 481, DateTimeKind.Local).AddTicks(8250) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 481, DateTimeKind.Local).AddTicks(9987), "https://s3.amazonaws.com/uifaces/faces/twitter/jarjan/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 481, DateTimeKind.Local).AddTicks(9995) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(33), "https://s3.amazonaws.com/uifaces/faces/twitter/carlyson/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(36) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(55), "https://s3.amazonaws.com/uifaces/faces/twitter/dreizle/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(59) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(78), "https://s3.amazonaws.com/uifaces/faces/twitter/rohixx/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(86) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(101), "https://s3.amazonaws.com/uifaces/faces/twitter/krasnoukhov/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(104) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(123), "https://s3.amazonaws.com/uifaces/faces/twitter/michzen/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(127) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(169), "https://s3.amazonaws.com/uifaces/faces/twitter/johnsmithagency/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(172) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(191), "https://s3.amazonaws.com/uifaces/faces/twitter/buryaknick/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(195) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(210), "https://s3.amazonaws.com/uifaces/faces/twitter/saschadroste/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(218) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(233), "https://s3.amazonaws.com/uifaces/faces/twitter/ryhanhassan/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(237) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(255), "https://s3.amazonaws.com/uifaces/faces/twitter/GavicoInd/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(259) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(274), "https://s3.amazonaws.com/uifaces/faces/twitter/davecraige/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(282) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(297), "https://s3.amazonaws.com/uifaces/faces/twitter/ramanathan_pdy/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(301) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(320), "https://s3.amazonaws.com/uifaces/faces/twitter/aoimedia/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(323) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(339), "https://s3.amazonaws.com/uifaces/faces/twitter/taylorling/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(346) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(361), "https://s3.amazonaws.com/uifaces/faces/twitter/posterjob/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(365) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(380), "https://s3.amazonaws.com/uifaces/faces/twitter/oskarlevinson/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(384) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(403), "https://s3.amazonaws.com/uifaces/faces/twitter/dpmachado/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(407) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(425), "https://s3.amazonaws.com/uifaces/faces/twitter/nandini_m/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(429) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(663), "https://picsum.photos/640/480/?image=1076", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(1739) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2053), "https://picsum.photos/640/480/?image=399", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2068) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2109), "https://picsum.photos/640/480/?image=132", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2117) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2211), "https://picsum.photos/640/480/?image=350", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2219) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2253), "https://picsum.photos/640/480/?image=492", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2260) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2294), "https://picsum.photos/640/480/?image=924", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2302) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2336), "https://picsum.photos/640/480/?image=1000", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2343) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2374), "https://picsum.photos/640/480/?image=452", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2381) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2415), "https://picsum.photos/640/480/?image=682", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2423) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2453), "https://picsum.photos/640/480/?image=232", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2460) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2494), "https://picsum.photos/640/480/?image=867", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2502) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2532), "https://picsum.photos/640/480/?image=853", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2540) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2574), "https://picsum.photos/640/480/?image=502", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2581) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2615), "https://picsum.photos/640/480/?image=19", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2623) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2657), "https://picsum.photos/640/480/?image=946", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2661) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2706), "https://picsum.photos/640/480/?image=928", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2713) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2747), "https://picsum.photos/640/480/?image=490", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2755) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2789), "https://picsum.photos/640/480/?image=135", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2796) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2827), "https://picsum.photos/640/480/?image=397", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2834) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2868), "https://picsum.photos/640/480/?image=898", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2876) });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 735, DateTimeKind.Local).AddTicks(8837), 0, 14, new DateTime(2019, 6, 14, 20, 31, 38, 735, DateTimeKind.Local).AddTicks(9842), 12 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(865), 1, 8, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(876), 1 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(929), 1, 5, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(933), 13 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(963), 20, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(967), 3 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(993), 1, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(997), 7 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1024), 1, 4, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1027), 19 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1058), 19, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1061), 12 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1092), -1, 10, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1095), 13 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1122), 1, 6, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1126), 15 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1152), 6, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1156), 21 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1182), -1, 16, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1186), 6 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1212), -1, 16, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1216), 21 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1243), 6, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1246), 1 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1273), 7, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1277), 18 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1307), 0, 6, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1311), 16 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1337), 13, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1341), 10 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1416), 8, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1420), 10 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1450), 1, 12, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1454), 9 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1484), 1, 19, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1488), 9 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1515), 0, 8, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1518), 18 });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Neque rerum dolorum quo sit et aperiam vero quidem tempore.", new DateTime(2019, 6, 14, 20, 31, 38, 713, DateTimeKind.Local).AddTicks(2417), 33, new DateTime(2019, 6, 14, 20, 31, 38, 713, DateTimeKind.Local).AddTicks(3161) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Accusantium eos neque sit.", new DateTime(2019, 6, 14, 20, 31, 38, 713, DateTimeKind.Local).AddTicks(4580), 22, new DateTime(2019, 6, 14, 20, 31, 38, 713, DateTimeKind.Local).AddTicks(4599) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Natus quidem omnis sed.
Ex et aut accusantium numquam optio dolore.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(2426), 40, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(2494) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Vitae blanditiis sunt esse tenetur non iusto accusantium ut. Necessitatibus voluptas sunt corrupti et illo minus inventore architecto. Iure cupiditate quas sunt natus animi. Eligendi et quasi qui corporis iusto voluptatem placeat. Sit minus est.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(5595), 29, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(5610) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Quia ut recusandae qui ut consectetur modi.
Temporibus dolorem quis iste tenetur sint vero aliquid culpa quod.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(5750), 23, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(5753) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "officia", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6460), 27, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6467) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 11, @"Repellat qui et.
Debitis commodi asperiores.
Numquam nam et nihil qui pariatur.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6580), new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6588) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 1, "Adipisci dolores quas debitis aliquam nulla qui ea aut atque.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6648), new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6652) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "doloribus", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6694), 39, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6697) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "non", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6724), 23, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6728) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "A quia facilis est.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6769), 34, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6773) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Amet architecto inventore dolorem perspiciatis non.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6848), 31, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6852) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "aut", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6882), 31, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6886) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Aut adipisci quas repudiandae quas similique et.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6947), 27, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6950) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "qui", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6977), 40, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6981) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Similique ipsum ut voluptatem.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7022), 30, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7026) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, @"Ducimus impedit fuga aut sit.
Error sit porro dignissimos ipsam.
Sit omnis vel consequatur et officiis.
Et quis perspiciatis eaque voluptatem rerum et quidem.
Blanditiis voluptatem numquam nostrum.
Ipsum laudantium autem rerum.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7173), 31, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7177) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "harum", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7207), 29, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7211) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Iure voluptate qui numquam expedita nihil qui explicabo sapiente.
Tempore voluptatum eius nihil eligendi quia error eligendi incidunt accusantium.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7302), 25, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7305) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Et minima eveniet.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7343), 33, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7347) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2019, 6, 14, 20, 31, 38, 535, DateTimeKind.Local).AddTicks(3706), "Johanna14@hotmail.com", "0nFK/ulgZxcWwlNKkiGrpo9QTqjTTotI0U5APDM+PDg=", "Wzu324yb4hl3OzpLnJXiv8jqzG87Sp2hmNNjcFkd51w=", new DateTime(2019, 6, 14, 20, 31, 38, 535, DateTimeKind.Local).AddTicks(5076), "Joana.Corwin" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2019, 6, 14, 20, 31, 38, 544, DateTimeKind.Local).AddTicks(1815), "Ron_Wuckert94@yahoo.com", "+LM3ikTVyBPW+EQVXjmihO86jQgYmiohkX9sCvNeO3o=", "YU7004Cq31zvU+pQjaEf8cvXKEsCjDrSgUWPj07mC9k=", new DateTime(2019, 6, 14, 20, 31, 38, 544, DateTimeKind.Local).AddTicks(1849), "Jayne_Schroeder31" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2019, 6, 14, 20, 31, 38, 551, DateTimeKind.Local).AddTicks(7395), "Emilio.OReilly52@gmail.com", "0nO6zTJbq8hZFkY4RPKBf6fV2CsCSBHtEdeN+QyuOxs=", "jq61q6DecpXbrKArZlO1MbTXYBo3h4bITM5glGx6SJA=", new DateTime(2019, 6, 14, 20, 31, 38, 551, DateTimeKind.Local).AddTicks(7418), "Wendy87" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 14, 20, 31, 38, 560, DateTimeKind.Local).AddTicks(2268), "Esther27@gmail.com", "OBdmBEmUC21xRgfz9Ir/Y3/hTQFyNmYscFr+K+pWW40=", "/w7ZOulgFjkq5c4b2Ewhxi8ye6MvBXUiNcnQ7UPiz2g=", new DateTime(2019, 6, 14, 20, 31, 38, 560, DateTimeKind.Local).AddTicks(2302), "Shakira_Reichel0" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2019, 6, 14, 20, 31, 38, 567, DateTimeKind.Local).AddTicks(9721), "Erica_Stanton@gmail.com", "PkSDSZTK3xmjEKuiZdGXpgyZgVEkZUTIQtkqoKA+I1A=", "VCGoKAXcBCEf5BGEhBD75jHhUD2tDxZN7DsqOyPcqQs=", new DateTime(2019, 6, 14, 20, 31, 38, 567, DateTimeKind.Local).AddTicks(9789), "Dandre.Osinski85" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 14, 20, 31, 38, 576, DateTimeKind.Local).AddTicks(5228), "Darrin.King@hotmail.com", "YRmjFzb+AaX3THJPmBwfMB2BOnYS/Wjnm2uGa5fAQ+M=", "Jy9SwzslD+Ai7/r/TQ+rWcbbtkI+8nN5x2vfe76HPqc=", new DateTime(2019, 6, 14, 20, 31, 38, 576, DateTimeKind.Local).AddTicks(5262), "Wayne40" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2019, 6, 14, 20, 31, 38, 584, DateTimeKind.Local).AddTicks(2905), "Theresa95@yahoo.com", "NiutEov71hHNV+nWuA9vHEnKygZKQYiuR5fld5gAcko=", "srJXtpNurgedI/tM87ChP0EutrR9MaPo6LlSB+NvG4E=", new DateTime(2019, 6, 14, 20, 31, 38, 584, DateTimeKind.Local).AddTicks(2935), "Luisa_Block47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2019, 6, 14, 20, 31, 38, 592, DateTimeKind.Local).AddTicks(4277), "Karli88@hotmail.com", "XgaRQuX1dTWR6652EgPQK6wkxztbEyo3AYIfXtpptUk=", "LFbRfkEf1MqXtIwxsz0EAvC+ePrN6hn7CK5QuufBPRs=", new DateTime(2019, 6, 14, 20, 31, 38, 592, DateTimeKind.Local).AddTicks(4326), "Gust.Leannon89" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2019, 6, 14, 20, 31, 38, 600, DateTimeKind.Local).AddTicks(6140), "Stephanie_Kemmer22@hotmail.com", "mAwoCl2FZ2O4AaVnG8Q0+HjgWuVrUHA70MZpmtb/b/8=", "vj+5gGjGJBRo/tS4U6d8EW4Snh4+n8KRjR7ddNTBgYo=", new DateTime(2019, 6, 14, 20, 31, 38, 600, DateTimeKind.Local).AddTicks(6178), "Fiona16" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2019, 6, 14, 20, 31, 38, 609, DateTimeKind.Local).AddTicks(160), "Piper_Lemke@yahoo.com", "JYveRXU4gN3SmuL7+STQyE6kkx/aYJIsqVJyLuHh37I=", "zzcRWtF/Z1HvADuvCMpZUEzXKYWpbxgBGJmvEdj1NMw=", new DateTime(2019, 6, 14, 20, 31, 38, 609, DateTimeKind.Local).AddTicks(209), "Justyn86" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 617, DateTimeKind.Local).AddTicks(2242), "Marquise.Cruickshank@gmail.com", "NlGCtYMVjv8h8lJNhLBPTbtZdz4rNAp4skSAM9knxaU=", "+omX6wycUe4OigLG9Om9c++dDlfOIzV0eLY7vr7NpRI=", new DateTime(2019, 6, 14, 20, 31, 38, 617, DateTimeKind.Local).AddTicks(2280), "Lela_Huels54" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 14, 20, 31, 38, 625, DateTimeKind.Local).AddTicks(3335), "Willow_Kilback4@yahoo.com", "lWKfFAHq89uy3DNd2KlQkTNNKzjgfvRfVNlgdamlNVs=", "AcsbGKmXnJEKNc3+5HDx8HRh4rkLdcITxvvDbQriUvU=", new DateTime(2019, 6, 14, 20, 31, 38, 625, DateTimeKind.Local).AddTicks(3373), "Sadye.Murazik" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2019, 6, 14, 20, 31, 38, 633, DateTimeKind.Local).AddTicks(739), "Carolyn.Franecki71@yahoo.com", "E2ZLwUVJnh9YMnYpvr9tEW7Udc1/x0BjjXhP6vsrNpc=", "2NfFuYNKzuHT5Bn/ZfvFEjhnnDQ4zeqZ17TY35ILN8c=", new DateTime(2019, 6, 14, 20, 31, 38, 633, DateTimeKind.Local).AddTicks(773), "Joshuah_Deckow" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2019, 6, 14, 20, 31, 38, 640, DateTimeKind.Local).AddTicks(8767), "Orland_Gusikowski55@yahoo.com", "9ibj/hSmlVrzaB66wrS4v/S2G4PrzPUeKCzDKnkGnFw=", "/8RyDXiUpvSj8clRo3iy/R3PKG0qs8ELQHygw6Bxh8w=", new DateTime(2019, 6, 14, 20, 31, 38, 640, DateTimeKind.Local).AddTicks(8800), "Nels18" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2019, 6, 14, 20, 31, 38, 649, DateTimeKind.Local).AddTicks(3590), "Celine41@yahoo.com", "J8qNE/AXwLVDB7PQfd3kmpBrSeKlAH62gsvBkcKRTlg=", "H5AHKTFoXfy+MIRrYtzpAQ51YYRylfDiwrm0cJ2CraQ=", new DateTime(2019, 6, 14, 20, 31, 38, 649, DateTimeKind.Local).AddTicks(3624), "Leonel_Glover" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2019, 6, 14, 20, 31, 38, 656, DateTimeKind.Local).AddTicks(9707), "Fabiola41@yahoo.com", "t18bsqutUQZPMC/s2CVaWUnub9+qTbNAiqhsRheOO0s=", "WnFvzYbPFPcKV41Nk9630osYA0VPzxxltQhPxr+xV6Q=", new DateTime(2019, 6, 14, 20, 31, 38, 656, DateTimeKind.Local).AddTicks(9892), "Tom46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2019, 6, 14, 20, 31, 38, 667, DateTimeKind.Local).AddTicks(3916), "Adrianna.Green@gmail.com", "ac9Puj0qCb3N0fHqmdb0H551dxrvEjzhLS+J0/rbsJs=", "DMlGoIgn1hoIchEA1jAdbDh/Q9hVWlRnWlcLP78QJKc=", new DateTime(2019, 6, 14, 20, 31, 38, 667, DateTimeKind.Local).AddTicks(3950), "Shirley91" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2019, 6, 14, 20, 31, 38, 676, DateTimeKind.Local).AddTicks(7231), "Jerome.Jenkins98@hotmail.com", "ptRC0Zk3NvFri14vOme2OOqbiqEBxsodzuEoG1ZZgx8=", "hMuURpNsu08ExiZ3MiYdugaLtBfviBEVYQplYZd6f3E=", new DateTime(2019, 6, 14, 20, 31, 38, 676, DateTimeKind.Local).AddTicks(7273), "Eleanore.Veum" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2019, 6, 14, 20, 31, 38, 684, DateTimeKind.Local).AddTicks(5126), "Oliver_OConner75@yahoo.com", "uJsP7ziCHg0+8yNSm91iGzY1kfftcsVnb6UT9GE796k=", "4IIt8aQ9VhRP+cfNryt1wF9q20LiGklxf0/7h48UCj0=", new DateTime(2019, 6, 14, 20, 31, 38, 684, DateTimeKind.Local).AddTicks(5160), "Rey75" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 692, DateTimeKind.Local).AddTicks(9334), "Anissa.Carter53@hotmail.com", "3b14K7H7kBmmimDRl5V9NWQQ1f1ODDkmszNTlaF0UMs=", "k47qFLVY5tyqnmygx5BufG86gVMG7yArQjS55qaqW2I=", new DateTime(2019, 6, 14, 20, 31, 38, 692, DateTimeKind.Local).AddTicks(9372), "Bridgette_Powlowski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 700, DateTimeKind.Local).AddTicks(8098), "hKqfEGGijPt2ZR/7y4fJJbd66q6NDcTRTYjsicH9Wqc=", "QlcBKr5wMuc2pIMQcVj3GTLcjgma7oDH9zddQKDZEtk=", new DateTime(2019, 6, 14, 20, 31, 38, 700, DateTimeKind.Local).AddTicks(8098) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Comments");

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 3, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(4498), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(5635), 21 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 8, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9233), 0, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9256), 18 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 9, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9339), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9346), 14 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 11, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9410), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9418), 8 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 9, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9467), 0, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9475), 6 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 10, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9524), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9531), 9 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 6, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9584), -1, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9592), 7 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 3, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9644), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9652), 2 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 20, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9712), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9720), 12 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 8, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9780), 1, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9792), 21 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 8, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9837), new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9845), 8 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 8, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9894), 0, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9901), 7 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 2, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9950), 0, new DateTime(2019, 6, 9, 12, 1, 7, 595, DateTimeKind.Local).AddTicks(9958), 15 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 19, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(7), new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(14), 9 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt" },
                values: new object[] { 1, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(94), 1, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(105) });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 6, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(154), new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(165), 13 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 2, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(211), new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(218), 15 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(267), new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(275), 6 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(332), new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(339), 13 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 7, new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(388), new DateTime(2019, 6, 9, 12, 1, 7, 596, DateTimeKind.Local).AddTicks(400), 7 });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Id velit est quidem.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(1021), 5, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(2090) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Recusandae ullam corporis.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(3763), 12, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(3781) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Similique itaque est aliquam aut amet dicta.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(3925), 12, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(3936) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Atque et dolor quaerat quos voluptates eos saepe.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4072), 14, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4084) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Dolores voluptatibus repellat sunt.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4182), 11, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4189) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Et amet nobis.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4276), 18, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4287) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Eum porro non non ipsum porro.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4435), 11, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4446) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 3, "Maxime dolorem aut.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4586), new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4593) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Accusantium nesciunt voluptate et sint in et.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4744), 15, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4756) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Quibusdam est aperiam odio voluptate voluptatibus nam facere.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4873), 18, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4876) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Vel quia est fugiat fugiat cumque consequuntur labore fugiat.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4990), 4, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(4997) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Atque numquam accusantium ad sint tenetur quae.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5084), 16, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5092) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Dolore quis et et et animi omnis et aliquam.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5239), 2, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5246) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Facilis hic nobis quasi dicta cumque corporis porro eveniet.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5337), 13, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5345) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Omnis modi vel.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5409), 15, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5416) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Quis non consequatur culpa sapiente aliquid voluptas aperiam harum.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5515), 10, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5522) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Nemo commodi cupiditate soluta sunt.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5594), 12, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5601) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Accusamus ut facilis ab ut magni maxime aut dolore.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5752), 5, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5764) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Possimus blanditiis cupiditate.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5832), 16, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5839) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Doloribus sit nemo.", new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5903), 2, new DateTime(2019, 6, 9, 12, 1, 7, 558, DateTimeKind.Local).AddTicks(5911) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 267, DateTimeKind.Local).AddTicks(1696), "https://s3.amazonaws.com/uifaces/faces/twitter/darylws/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 267, DateTimeKind.Local).AddTicks(9418) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(589), "https://s3.amazonaws.com/uifaces/faces/twitter/sangdth/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(604) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(638), "https://s3.amazonaws.com/uifaces/faces/twitter/eugeneeweb/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(642) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(661), "https://s3.amazonaws.com/uifaces/faces/twitter/djsherman/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(668) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(683), "https://s3.amazonaws.com/uifaces/faces/twitter/scrapdnb/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(687) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(706), "https://s3.amazonaws.com/uifaces/faces/twitter/guiiipontes/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(710) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(729), "https://s3.amazonaws.com/uifaces/faces/twitter/tanveerrao/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(732) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(751), "https://s3.amazonaws.com/uifaces/faces/twitter/ecommerceil/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(755) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(770), "https://s3.amazonaws.com/uifaces/faces/twitter/clubb3rry/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(778) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(793), "https://s3.amazonaws.com/uifaces/faces/twitter/shojberg/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(797) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(815), "https://s3.amazonaws.com/uifaces/faces/twitter/mtolokonnikov/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(819) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(834), "https://s3.amazonaws.com/uifaces/faces/twitter/catarino/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(838) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(857), "https://s3.amazonaws.com/uifaces/faces/twitter/hasslunsford/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(861) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(880), "https://s3.amazonaws.com/uifaces/faces/twitter/a_harris88/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(883) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(902), "https://s3.amazonaws.com/uifaces/faces/twitter/sydlawrence/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(906) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(925), "https://s3.amazonaws.com/uifaces/faces/twitter/dreizle/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(929) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(944), "https://s3.amazonaws.com/uifaces/faces/twitter/constantx/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(951) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(966), "https://s3.amazonaws.com/uifaces/faces/twitter/ariil/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(970) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(989), "https://s3.amazonaws.com/uifaces/faces/twitter/andresenfredrik/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(993) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(1012), "https://s3.amazonaws.com/uifaces/faces/twitter/atanism/128.jpg", new DateTime(2019, 6, 9, 12, 1, 7, 268, DateTimeKind.Local).AddTicks(1016) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(2131), "https://picsum.photos/640/480/?image=530", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(2848) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3067), "https://picsum.photos/640/480/?image=63", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3079) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3173), "https://picsum.photos/640/480/?image=468", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3180) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3199), "https://picsum.photos/640/480/?image=128", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3203) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3222), "https://picsum.photos/640/480/?image=139", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3226) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3245), "https://picsum.photos/640/480/?image=683", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3248) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3267), "https://picsum.photos/640/480/?image=125", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3271) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3290), "https://picsum.photos/640/480/?image=309", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3294) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3313), "https://picsum.photos/640/480/?image=626", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3316) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3335), "https://picsum.photos/640/480/?image=349", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3339) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3358), "https://picsum.photos/640/480/?image=518", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3362) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3381), "https://picsum.photos/640/480/?image=164", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3384) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3399), "https://picsum.photos/640/480/?image=16", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3403) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3422), "https://picsum.photos/640/480/?image=53", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3426) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3498), "https://picsum.photos/640/480/?image=524", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3501) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3520), "https://picsum.photos/640/480/?image=458", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3524) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3543), "https://picsum.photos/640/480/?image=1015", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3547) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3566), "https://picsum.photos/640/480/?image=354", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3569) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3588), "https://picsum.photos/640/480/?image=783", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3592) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3611), "https://picsum.photos/640/480/?image=667", new DateTime(2019, 6, 9, 12, 1, 7, 274, DateTimeKind.Local).AddTicks(3615) });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 578, DateTimeKind.Local).AddTicks(8159), -1, 12, new DateTime(2019, 6, 9, 12, 1, 7, 578, DateTimeKind.Local).AddTicks(9322), 6 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2437), -1, 11, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2463), 6 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2580), -1, 7, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2592), 14 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2675), 19, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2686), 19 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2769), -1, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2777), 6 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2860), -1, 13, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2875), 18 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2966), 16, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(2977), 17 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3339), 0, 12, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3362), 9 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3664), -1, 8, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3672), 18 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3758), 2, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3770), 11 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3849), 0, 17, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3860), 12 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3940), 0, 17, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(3951), 19 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4011), 10, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4027), 7 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4098), 4, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4106), 8 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4159), -1, 18, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4166), 20 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4242), 18, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4257), 13 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4332), 4, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4344), 9 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4431), -1, 6, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4442), 12 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4525), 0, 17, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4540), 10 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4627), -1, 9, new DateTime(2019, 6, 9, 12, 1, 7, 579, DateTimeKind.Local).AddTicks(4638), 21 });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Libero dolor unde sint unde qui similique quia in inventore.", new DateTime(2019, 6, 9, 12, 1, 7, 545, DateTimeKind.Local).AddTicks(8851), 21, new DateTime(2019, 6, 9, 12, 1, 7, 545, DateTimeKind.Local).AddTicks(9617) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "maiores", new DateTime(2019, 6, 9, 12, 1, 7, 546, DateTimeKind.Local).AddTicks(1562), 23, new DateTime(2019, 6, 9, 12, 1, 7, 546, DateTimeKind.Local).AddTicks(1581) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "voluptas", new DateTime(2019, 6, 9, 12, 1, 7, 546, DateTimeKind.Local).AddTicks(1656), 30, new DateTime(2019, 6, 9, 12, 1, 7, 546, DateTimeKind.Local).AddTicks(1664) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, @"Et veritatis qui quo cum.
Enim minus similique aut ab non in in temporibus eos.
Autem voluptas tenetur.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(266), 40, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(285) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "quo", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(368), 31, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(375) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Optio nulla reprehenderit. Vel non vero explicabo odit saepe. Dicta quia iusto rerum ratione. Natus mollitia velit totam voluptate eius porro sit autem. Saepe laborum velit reiciendis a sed quo officiis. Expedita eum voluptas.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(3924), 33, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(3940) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 13, "Culpa accusamus libero unde non est. Eos ea commodi iste quis fuga tempore ab. Ullam sapiente atque voluptates repudiandae ullam. Laborum natus id quos rerum quibusdam hic. Cumque qui id eos praesentium nihil. Consequatur et blanditiis.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4223), new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4230) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 9, "sequi", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4264), new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4268) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Ut ut minima quae similique. Voluptatem inventore maiores molestiae velit. Nisi velit rerum at quo necessitatibus sed officia minus. Mollitia error qui architecto. Vel blanditiis reiciendis consectetur molestias neque totam id. Nihil voluptate dolorem vero qui est aspernatur.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4487), 29, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4491) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, @"Veniam corporis placeat occaecati.
Consectetur mollitia dolores autem quo deserunt.
Culpa itaque animi aut minus eligendi.
Officiis aut vel molestiae molestiae quisquam soluta dignissimos.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4770), 22, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4778) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "nulla", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4812), 36, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4816) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Tempora tempore inventore ut.
Rem magni impedit illum cum minima sed ab dicta.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4963), 35, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(4974) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "fuga", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5080), 28, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5087) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Voluptas placeat excepturi sit modi accusamus et repudiandae veritatis. Ducimus repellendus veritatis harum distinctio repellat suscipit dolore quisquam. Quae ea labore aperiam doloremque dignissimos illum. A dolores consectetur minima et officiis exercitationem modi commodi.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5318), 30, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5321) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Numquam commodi animi corporis est reprehenderit consequatur fuga ipsa. Sequi impedit facere commodi molestias deleniti maiores dolor. Ipsa a non.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5465), 34, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5469) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Odio quidem aut suscipit aut. Veniam amet debitis et qui autem nihil. Sit sunt non consequuntur adipisci quia.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5571), 39, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5574) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Temporibus aut aut quis. Iusto nihil laudantium illo non maiores nesciunt quia. Aut totam quia et ab. Maiores fuga est fuga quo.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5722), 23, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5726) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, @"Quisquam facere esse quia explicabo fuga.
Ut ipsum repellat nemo sit quis.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5805), 30, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5809) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Vitae et possimus dicta fugiat non qui neque quia. Qui qui reprehenderit id. Facilis quia ut maxime sed quia.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5986), 30, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(5994) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Illo hic et aliquam amet veniam.", new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(6062), 39, new DateTime(2019, 6, 9, 12, 1, 7, 547, DateTimeKind.Local).AddTicks(6065) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 9, 12, 1, 7, 314, DateTimeKind.Local).AddTicks(1895), "Joaquin70@gmail.com", "ky9kx3wOsL6x8EndxkxU/gEcvxpMV6BVAQR8s93WGZ8=", "05g1L/iALYmDEXVbgWW9425sceGegaAdkvvQVrAPyb8=", new DateTime(2019, 6, 9, 12, 1, 7, 314, DateTimeKind.Local).AddTicks(2703), "Maureen32" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2019, 6, 9, 12, 1, 7, 322, DateTimeKind.Local).AddTicks(6843), "Delfina.Purdy34@yahoo.com", "JgG11NZ80BMdWXJBTGV4reS/48eX9fV9QD3NadJU3Y4=", "vEi/Ha6oYsFxQu5XY8MT4CF9TMdm3JYLZO0MgIe7jE8=", new DateTime(2019, 6, 9, 12, 1, 7, 322, DateTimeKind.Local).AddTicks(6877), "Wade1" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2019, 6, 9, 12, 1, 7, 331, DateTimeKind.Local).AddTicks(1429), "Tod_Hilpert@gmail.com", "BdG3mUmIysdcpEW0jbSdvVuUZ7FAD5e2TpQJxnxwBhU=", "bc1la/1ktIoSGjYr5y9EmDzOJj58PzWmplqZLn+EFwQ=", new DateTime(2019, 6, 9, 12, 1, 7, 331, DateTimeKind.Local).AddTicks(1441), "Roscoe.Ziemann" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2019, 6, 9, 12, 1, 7, 342, DateTimeKind.Local).AddTicks(1282), "Demetrius17@hotmail.com", "1bPNHDVcgoAHPT+Fb7iZXC8UlrncRQn9HBQLPD6r/p8=", "X6EsvN7kF+yGPGDmLJ2zHtlyxJLHlGpV++A1ZiP91fM=", new DateTime(2019, 6, 9, 12, 1, 7, 342, DateTimeKind.Local).AddTicks(1316), "Jaleel66" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 9, 12, 1, 7, 355, DateTimeKind.Local).AddTicks(3938), "Neva.Koss@yahoo.com", "VllqrWUVLOHWUxaaObs0vbb9w3iFcNSVZYZi7qHfN7g=", "Xy7DGYCaZMd78Fd+LxXkeJg3gGOz4Qd7OWTqy2Fdf8Q=", new DateTime(2019, 6, 9, 12, 1, 7, 355, DateTimeKind.Local).AddTicks(3968), "Judson92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2019, 6, 9, 12, 1, 7, 364, DateTimeKind.Local).AddTicks(1714), "Brooke.Quigley@hotmail.com", "jT+5XHBUQXAWLOqzar4CHIkYtaDCI8cwxq7EeW3pehs=", "UrrS8vbDqk6UnQ5oXeB6M7OW3xQyY289pTGxfhakF+k=", new DateTime(2019, 6, 9, 12, 1, 7, 364, DateTimeKind.Local).AddTicks(1756), "Telly.Hackett" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 9, 12, 1, 7, 376, DateTimeKind.Local).AddTicks(9507), "Geo98@gmail.com", "G0kPxjRDhnvqLvPrgc477BRlCgHFNPPSB+tMrgG35f0=", "iXzBPHu1JRM7vQ9qtv0QXAy/Nu7Ezo3qtuy/zcNsIe8=", new DateTime(2019, 6, 9, 12, 1, 7, 376, DateTimeKind.Local).AddTicks(9556), "Kristofer.Schaefer" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2019, 6, 9, 12, 1, 7, 391, DateTimeKind.Local).AddTicks(8066), "Fredrick_Gottlieb71@gmail.com", "kmcWD7KBY6Iid8PLFrtlThDTJAWyniq8U5ZLFYMiaLo=", "Ih/rD2Giiosd7c1l1X/Cd7t73WObquqpA6VWPLMt+WE=", new DateTime(2019, 6, 9, 12, 1, 7, 391, DateTimeKind.Local).AddTicks(8115), "Stacey51" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2019, 6, 9, 12, 1, 7, 402, DateTimeKind.Local).AddTicks(3547), "Rebeca_Nikolaus87@yahoo.com", "dWTzWJBDFZA1YT/GPSva69M6Jiy5K0X+4Zi+fONql5w=", "JsLICz8r9/obefiMYXvXqCuJHAnRR3CRFaxpV0IrDlY=", new DateTime(2019, 6, 9, 12, 1, 7, 402, DateTimeKind.Local).AddTicks(3581), "Ronny81" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2019, 6, 9, 12, 1, 7, 411, DateTimeKind.Local).AddTicks(2146), "Melvina_White15@yahoo.com", "zuvrH7Cfy0lnCJOpWtDgwb81lo3HhnH8zYk5l6I2oQE=", "DNl0ooLJAwWLAJX3NPbLIGTy8K7dYqdRSrzQh8C0N0U=", new DateTime(2019, 6, 9, 12, 1, 7, 411, DateTimeKind.Local).AddTicks(2180), "Ariel.Krajcik" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 419, DateTimeKind.Local).AddTicks(8276), "Darian_Quigley@yahoo.com", "g8iN35AuyFLiBCBgB6Fyr7AB4BLhuO3WHmLu1Mwt7r4=", "Uxl4KoemTeIY10sgHugVTpz0bECFHdS1f60O50RFv94=", new DateTime(2019, 6, 9, 12, 1, 7, 419, DateTimeKind.Local).AddTicks(8318), "Soledad.Price" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2019, 6, 9, 12, 1, 7, 428, DateTimeKind.Local).AddTicks(4256), "Frederique81@yahoo.com", "qWQ8AGCyIrZATHjYBhq+V+GaIVBSs0nF5Q1pntZksWw=", "SfOB801X6p8x7m4Zzy3D0hdp6N/mnail8DPKeK/5wlQ=", new DateTime(2019, 6, 9, 12, 1, 7, 428, DateTimeKind.Local).AddTicks(4282), "Cornell49" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2019, 6, 9, 12, 1, 7, 437, DateTimeKind.Local).AddTicks(9633), "Jordon_Armstrong@gmail.com", "tBXA9OKuIfZVvfggsWWuUxvuKexf/TO/uC4q2EZc4rw=", "QSW16kNtVz/HcL2wqEbBw14yEh7nJutTctiuQ+Z1luk=", new DateTime(2019, 6, 9, 12, 1, 7, 437, DateTimeKind.Local).AddTicks(9678), "Orion.Harvey" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2019, 6, 9, 12, 1, 7, 446, DateTimeKind.Local).AddTicks(3267), "Alexandrine_Waelchi73@hotmail.com", "rNC0UpCWKjkrUNnwdCWTFxYYkoYNp/+HkQNQuxHQ20A=", "r2DRU2zD4DG6FdoWkxflGo4hXKgj2rmFJKEAQtlHKU0=", new DateTime(2019, 6, 9, 12, 1, 7, 446, DateTimeKind.Local).AddTicks(3305), "Delilah_Kris70" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2019, 6, 9, 12, 1, 7, 459, DateTimeKind.Local).AddTicks(3835), "Penelope.Nienow@yahoo.com", "KiOd6tpx0iQ22viH36Q9HU4eg7PYTYZXHuC9FyBaDL8=", "w2y0gCNnu2IDexEWdjtZus8raptpH9WQCbuqDwEEx2o=", new DateTime(2019, 6, 9, 12, 1, 7, 459, DateTimeKind.Local).AddTicks(3884), "Madonna_Lindgren" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2019, 6, 9, 12, 1, 7, 471, DateTimeKind.Local).AddTicks(8874), "Ara_Willms21@gmail.com", "swVrE0lgSNtlB5b5vzXFDWmWB1dVUji8ZHdxTc8lzxI=", "tihI/4B2kLiR7qT1HilyS24lXIbkjTIVk/kArC3wf/Y=", new DateTime(2019, 6, 9, 12, 1, 7, 471, DateTimeKind.Local).AddTicks(8942), "Gene_Schmeler59" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2019, 6, 9, 12, 1, 7, 487, DateTimeKind.Local).AddTicks(7229), "Charley_Gerlach32@gmail.com", "FiAdy7zCoUyNakFlxvS4gMcJcGGiCDfMvAyNUelbSlM=", "vxNPazXQx2GppfVhHclEH761FUZgPxyOI+3mgk/2W+E=", new DateTime(2019, 6, 9, 12, 1, 7, 487, DateTimeKind.Local).AddTicks(7278), "Muriel.Pagac97" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2019, 6, 9, 12, 1, 7, 499, DateTimeKind.Local).AddTicks(4185), "Kimberly.Flatley@hotmail.com", "2LnsQrZUUcEhPPCqJbbkW/9Po1cWLmTnYwvqwmDvGUU=", "P1VYEjVhwgT2hApAuwyCBQv2e7nRAwkuaweYSrbHYp4=", new DateTime(2019, 6, 9, 12, 1, 7, 499, DateTimeKind.Local).AddTicks(4222), "Marjorie.Mills" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2019, 6, 9, 12, 1, 7, 512, DateTimeKind.Local).AddTicks(886), "Judah.Hackett@hotmail.com", "JsyqJEwYtEoK8NdgY1AzLYAJSEQUSUXOb2DmkgNxviY=", "j2+gElzhfaQQJzAnb4CRZdOR28Bbvxoexlo/u2rQ9h8=", new DateTime(2019, 6, 9, 12, 1, 7, 512, DateTimeKind.Local).AddTicks(935), "Morgan.Jones52" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 524, DateTimeKind.Local).AddTicks(6907), "Coy29@yahoo.com", "XeYRc0vSmE5lQQjCTd6E4JDR6/sUhapz3Q249gUQRYY=", "n2CgoxIyxZ2AvnRWmMK2jndVtmtTNZSTxk1jcVoS7Jg=", new DateTime(2019, 6, 9, 12, 1, 7, 524, DateTimeKind.Local).AddTicks(6949), "Lucienne_Jerde" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 9, 12, 1, 7, 532, DateTimeKind.Local).AddTicks(8721), "c+fl6yzl+ooS+Vn/sEP3leUPy60WaLJsMP0nmO+B/bY=", "MxFf4jpD6JI1XIG7ZElNeZz3YpeOEw+NENQ5ud5q67I=", new DateTime(2019, 6, 9, 12, 1, 7, 532, DateTimeKind.Local).AddTicks(8721) });
        }
    }
}
