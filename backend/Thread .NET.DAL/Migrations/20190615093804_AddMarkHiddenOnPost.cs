﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddMarkHiddenOnPost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsHidden",
                table: "Posts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 8, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(2340), -1, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(3578), 17 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 9, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(4911), new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(4930), 10 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5017), 1, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5028), 7 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 6, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5115), 1, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5126), 6 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 14, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5191), 0, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5198), 3 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5255), 0, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5262), 9 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 6, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5311), -1, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5315), 12 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 15, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5391), 0, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5398), 19 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 11, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5462), 0, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5474), 17 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 8, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5542), new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5553), 5 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 17, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5621), 0, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5632), 11 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5697), 0, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5708), 2 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 1, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5897), 1, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5908), 5 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 4, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5987), new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(5995), 1 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 2, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(6067), -1, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(6074), 13 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 10, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(6146), new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(6153), 2 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 11, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(6233), new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(6244), 16 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 4, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(6320), new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(6331), 19 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt" },
                values: new object[] { 18, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(6418), new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(6425) });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 14, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(6493), 1, new DateTime(2019, 6, 15, 12, 38, 2, 914, DateTimeKind.Local).AddTicks(6501), 15 });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Aperiam sint doloremque.", new DateTime(2019, 6, 15, 12, 38, 2, 857, DateTimeKind.Local).AddTicks(9067), 19, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(245) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Qui laborum officiis corrupti aut aut ut nesciunt consequatur debitis.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(2390), 1, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(2409) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Nulla harum est eaque sint et.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(2526), 1, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(2533) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Sapiente error non.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(2598), 13, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(2605) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Expedita necessitatibus amet nam.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(2669), 5, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(2673) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Modi in labore aut aspernatur ea itaque.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(2752), 4, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(2760) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Sed maiores ipsa laudantium enim aut fugit.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(2903), 12, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(2911) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Nihil est enim natus dolorem voluptas sed quas fugit eligendi.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3005), 4, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3013) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Nostrum voluptate voluptas et.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3077), 15, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3085) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Et sed et consequatur qui asperiores id nobis ut aut.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3175), 13, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3183) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Aliquam magni in sunt soluta.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3251), 16, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3258) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 8, "Iusto vel totam tenetur.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3383), new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3390) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Illo voluptatibus vitae sed ea mollitia voluptates necessitatibus pariatur.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3481), 4, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3489) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Est sint sequi aut dolorum consequatur molestiae asperiores neque.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3579), 5, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3587) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Est itaque qui.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3647), 4, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3655) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Illo ipsa quaerat pariatur et.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3723), 19, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3730) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Et nam nisi labore qui repellendus excepturi aliquid quae.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3821), 6, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3828) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Pariatur rerum nihil quod officia.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3949), 15, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(3957) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Deleniti quam est minima minus sint magnam quia in officiis.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(4047), 16, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(4055) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Facere voluptas voluptatem est.", new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(4119), 3, new DateTime(2019, 6, 15, 12, 38, 2, 858, DateTimeKind.Local).AddTicks(4127) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 315, DateTimeKind.Local).AddTicks(4158), "https://s3.amazonaws.com/uifaces/faces/twitter/emsgulam/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 316, DateTimeKind.Local).AddTicks(7218) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 316, DateTimeKind.Local).AddTicks(9752), "https://s3.amazonaws.com/uifaces/faces/twitter/wearesavas/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 316, DateTimeKind.Local).AddTicks(9778) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 316, DateTimeKind.Local).AddTicks(9937), "https://s3.amazonaws.com/uifaces/faces/twitter/2fockus/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 316, DateTimeKind.Local).AddTicks(9945) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(1), "https://s3.amazonaws.com/uifaces/faces/twitter/jomarmen/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(12) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(65), "https://s3.amazonaws.com/uifaces/faces/twitter/ponchomendivil/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(77) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(126), "https://s3.amazonaws.com/uifaces/faces/twitter/nickfratter/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(130) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(171), "https://s3.amazonaws.com/uifaces/faces/twitter/bryan_topham/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(179) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(220), "https://s3.amazonaws.com/uifaces/faces/twitter/phillapier/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(228) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(258), "https://s3.amazonaws.com/uifaces/faces/twitter/jamiebrittain/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(265) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(299), "https://s3.amazonaws.com/uifaces/faces/twitter/chrismj83/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(307) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(356), "https://s3.amazonaws.com/uifaces/faces/twitter/tanveerrao/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(364) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(405), "https://s3.amazonaws.com/uifaces/faces/twitter/jennyshen/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(416) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(458), "https://s3.amazonaws.com/uifaces/faces/twitter/ademilter/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(466) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(511), "https://s3.amazonaws.com/uifaces/faces/twitter/sulaqo/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(522) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(564), "https://s3.amazonaws.com/uifaces/faces/twitter/therealmarvin/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(575) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(617), "https://s3.amazonaws.com/uifaces/faces/twitter/olgary/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(628) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(669), "https://s3.amazonaws.com/uifaces/faces/twitter/goddardlewis/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(677) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(719), "https://s3.amazonaws.com/uifaces/faces/twitter/fronx/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(726) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(768), "https://s3.amazonaws.com/uifaces/faces/twitter/kinday/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(779) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(821), "https://s3.amazonaws.com/uifaces/faces/twitter/elenadissi/128.jpg", new DateTime(2019, 6, 15, 12, 38, 2, 317, DateTimeKind.Local).AddTicks(828) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(5196), "https://picsum.photos/640/480/?image=779", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(6684) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7178), "https://picsum.photos/640/480/?image=1061", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7201) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7277), "https://picsum.photos/640/480/?image=917", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7288) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7337), "https://picsum.photos/640/480/?image=855", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7348) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7401), "https://picsum.photos/640/480/?image=86", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7413) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7462), "https://picsum.photos/640/480/?image=258", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7473) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7522), "https://picsum.photos/640/480/?image=869", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7533) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7582), "https://picsum.photos/640/480/?image=539", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7594) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7647), "https://picsum.photos/640/480/?image=626", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7654) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7700), "https://picsum.photos/640/480/?image=822", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7707) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7752), "https://picsum.photos/640/480/?image=669", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7760) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7801), "https://picsum.photos/640/480/?image=881", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7813) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7858), "https://picsum.photos/640/480/?image=774", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7866) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7915), "https://picsum.photos/640/480/?image=1050", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7926) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7983), "https://picsum.photos/640/480/?image=584", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(7994) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(8032), "https://picsum.photos/640/480/?image=35", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(8039) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(8070), "https://picsum.photos/640/480/?image=173", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(8077) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(8107), "https://picsum.photos/640/480/?image=164", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(8111) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(8153), "https://picsum.photos/640/480/?image=1046", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(8160) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(8205), "https://picsum.photos/640/480/?image=420", new DateTime(2019, 6, 15, 12, 38, 2, 329, DateTimeKind.Local).AddTicks(8213) });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(5738), 1, 8, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(7030), 13 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(8721), -1, 7, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(8744), 4 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(8835), -1, 3, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(8842), 11 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(8895), 1, 6, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(8903), 11 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(8952), new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(8959), 13 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9012), 18, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9020), 18 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9073), 0, 5, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9080), 11 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9133), 1, 16, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9140), 9 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9193), 0, 10, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9201), 16 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9250), 0, 4, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9254), 3 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9307), 0, 14, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9314), 2 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9363), 0, 8, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9371), 7 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9420), 0, 12, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9427), 12 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9477), -1, 1, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9480) });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9529), 2, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9537), 11 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9575), 4, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9582), 11 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9650), 0, 19, new DateTime(2019, 6, 15, 12, 38, 2, 900, DateTimeKind.Local).AddTicks(9658), 8 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 901, DateTimeKind.Local).AddTicks(5), 9, new DateTime(2019, 6, 15, 12, 38, 2, 901, DateTimeKind.Local).AddTicks(16), 13 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 901, DateTimeKind.Local).AddTicks(73), -1, 5, new DateTime(2019, 6, 15, 12, 38, 2, 901, DateTimeKind.Local).AddTicks(81), 20 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 901, DateTimeKind.Local).AddTicks(126), 1, 9, new DateTime(2019, 6, 15, 12, 38, 2, 901, DateTimeKind.Local).AddTicks(134), 16 });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Dolor dolorem quis vero vero ut atque et amet.", new DateTime(2019, 6, 15, 12, 38, 2, 840, DateTimeKind.Local).AddTicks(9850), 22, new DateTime(2019, 6, 15, 12, 38, 2, 841, DateTimeKind.Local).AddTicks(824) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Quis et mollitia culpa sint enim.
Consectetur voluptate doloribus totam cumque eos voluptatem.
Nemo enim tempora sunt sint vel itaque.
Quasi repellendus id sint ut ratione quibusdam est neque.", new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(3137), 36, new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(3179) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Voluptatem ea eos. Deserunt omnis vel iure. Odit eius ut debitis. Qui voluptatum eveniet accusantium iusto debitis cupiditate autem enim. Voluptas ipsa est ut nemo itaque illum exercitationem est. Hic dolorem hic voluptatem.", new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(7223), 36, new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(7249) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Harum et exercitationem commodi enim perferendis voluptas nobis.", new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(7423), 38, new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(7430) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, @"Non sunt eum et unde voluptatem aut.
Numquam quis molestiae.
Expedita fugiat sunt cupiditate.
Labore quaerat deleniti voluptas neque vel praesentium.
Est mollitia earum dolorum quasi et possimus autem distinctio nihil.
Error qui labore voluptatem veniam animi esse non.", new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(7853), 28, new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(7861) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, @"Nesciunt nostrum dolor ad ducimus porro commodi ad molestiae.
Omnis iste ipsum praesentium qui corrupti dolorem eum asperiores velit.", new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(8034), 37, new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(8046) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "repellat", new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(9061), 25, new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(9077) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "placeat", new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(9167), 33, new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(9175) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Amet praesentium dolor sit deleniti ut. Inventore praesentium quidem unde praesentium totam fugit qui enim est. Aut dolorum assumenda sint.", new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(9386), 24, new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(9394) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Voluptatum laborum tempore quo. Dolorum qui voluptatibus alias. Totam nobis aperiam ab aliquid.", new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(9598), 28, new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(9605) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, @"Vitae aut deleniti aut rerum aut sed facilis enim.
Laborum ut odio aspernatur totam.
Repellendus et quam voluptas quia dolores.
Et aliquam sint assumenda.", new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(9832), 32, new DateTime(2019, 6, 15, 12, 38, 2, 842, DateTimeKind.Local).AddTicks(9839) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Dolore ad dolorem autem dolores autem hic.
Vitae et impedit quod.
Incidunt dolore itaque culpa et deserunt occaecati nesciunt.
Dignissimos hic sapiente sunt fuga quis sed delectus.
Minus iure voluptatem doloribus.
Recusandae explicabo enim asperiores.", new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(153), 38, new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(160) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "In sequi voluptatem enim fugiat doloribus. Rerum ex et unde eum eius velit. Dolorem in tenetur sed ad aut aliquid. Et vitae alias atque mollitia animi libero saepe. Dolorum occaecati natus esse id aut quis aut quo temporibus. Deserunt odio laboriosam.", new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(500), 35, new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(511) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, @"Enim quidem unde qui ab.
Dolor consectetur eum molestias repudiandae nisi.
Sed est quia optio at ratione natus voluptatum.
Ratione error sit voluptatem dolores et quod.
Aut pariatur est reprehenderit qui.
Architecto quasi excepturi maiores dolorem.", new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(829), 39, new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(836) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Dignissimos ut illo eum magni sit eum animi ut.", new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(942), 37, new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(953) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Ut qui exercitationem reiciendis ducimus perspiciatis cumque veritatis sunt exercitationem.", new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(1153), 40, new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(1161) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "et", new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(1221), 27, new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(1229) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, @"Soluta voluptas id.
Vitae aut laborum ut reiciendis dolore quas officia at vel.
Et dolor eligendi.
Totam alias voluptas minus officiis ab aut occaecati alias.", new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(1440), 28, new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(1448) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Labore eligendi consequatur numquam provident possimus non.", new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(1595), 21, new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(1603) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Illum quasi impedit aspernatur dolores eaque eum natus.", new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(1704), 30, new DateTime(2019, 6, 15, 12, 38, 2, 843, DateTimeKind.Local).AddTicks(1712) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 15, 12, 38, 2, 388, DateTimeKind.Local).AddTicks(3857), "Elenora30@hotmail.com", "onMPnKZuOFOvBdSBcR+mNPJGswwLjJD6nhT0/2oLRrU=", "mMETF0vubNxvFVkF2VTGRMlpt+/aBs7RSwCuBJ7U/k4=", new DateTime(2019, 6, 15, 12, 38, 2, 388, DateTimeKind.Local).AddTicks(5333), "Emely_Kozey" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2019, 6, 15, 12, 38, 2, 409, DateTimeKind.Local).AddTicks(4532), "Drake71@gmail.com", "ohbhoecJyU+eq9i6tfQiAeemk80ImkZA1qx5m83MEQI=", "eQzrDbkgi8faf/y7rNVbLBt09sGozT849mbN7Qo1jDU=", new DateTime(2019, 6, 15, 12, 38, 2, 409, DateTimeKind.Local).AddTicks(4596), "Andre42" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2019, 6, 15, 12, 38, 2, 428, DateTimeKind.Local).AddTicks(3134), "Rod.Torphy76@yahoo.com", "VgJN059qBlEv6Hu1al5/NaQrQAItIsWDqLRbnF6n67o=", "2fBaDOdI7HzBUcxDdUiDQCn2jdUjNym1Dojkwd0n074=", new DateTime(2019, 6, 15, 12, 38, 2, 428, DateTimeKind.Local).AddTicks(3195), "Ahmed.Botsford11" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2019, 6, 15, 12, 38, 2, 443, DateTimeKind.Local).AddTicks(2691), "Danielle_Langosh@hotmail.com", "w1JwKlK8aCrj1guy2nczXPXUfX1z37eUigfGn4Mw4l4=", "EiD6YUwtK9P7X9/ar+WH7z0+i+mPJpj4MuB0iSq+il8=", new DateTime(2019, 6, 15, 12, 38, 2, 443, DateTimeKind.Local).AddTicks(2740), "Phyllis_Dibbert57" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 15, 12, 38, 2, 459, DateTimeKind.Local).AddTicks(5473), "Aidan26@yahoo.com", "6+sBj8DAcGewrg1wgmEUA0MNFhMlPR3Vd8CBPJT7TfE=", "u9L1PxiBtmIkThRDIOMCcTtY+Sjv1XKRUG2s+uK/prA=", new DateTime(2019, 6, 15, 12, 38, 2, 459, DateTimeKind.Local).AddTicks(5519), "Olen86" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2019, 6, 15, 12, 38, 2, 474, DateTimeKind.Local).AddTicks(1228), "Geovany99@hotmail.com", "7ua+5ODt6PdAS7k4H9D+rzfEF2rn1EJW7DzTp7R+HWE=", "pnu/1jarjo0k0ZuQUVBFgky/fUEaZN4iFc+M2UjmuCU=", new DateTime(2019, 6, 15, 12, 38, 2, 474, DateTimeKind.Local).AddTicks(1345), "Cathrine_Wunsch40" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2019, 6, 15, 12, 38, 2, 494, DateTimeKind.Local).AddTicks(6152), "Rubye_Oberbrunner@yahoo.com", "zGTtGM7qon5QIFpOFm4m7tTzuuSFOe0Ok3A5U1d//Ec=", "aeSUb3Q1Ut3x88Ct7bU90ntYkuAMuBexaKaUPMSW7kc=", new DateTime(2019, 6, 15, 12, 38, 2, 494, DateTimeKind.Local).AddTicks(6273), "Jude22" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2019, 6, 15, 12, 38, 2, 508, DateTimeKind.Local).AddTicks(9554), "Adell60@hotmail.com", "YWTKZGEKMvHQXpIKSQpTQqv55i3eojQx+tRQRjhxLi8=", "WPk9Xqk9G0Hn1PChNXf09HQnxW2iba/S/G9ysRDXpD8=", new DateTime(2019, 6, 15, 12, 38, 2, 508, DateTimeKind.Local).AddTicks(9603), "Rebeca70" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2019, 6, 15, 12, 38, 2, 529, DateTimeKind.Local).AddTicks(2624), "Ned_Brekke12@yahoo.com", "PexZfnRQZlsbFpdffmyv/bP6t8BkMhfkmK23UiJMAGw=", "4Cq8NGa22KqG8KAju389rTbj0QHF4O5sHBE6FOfohuk=", new DateTime(2019, 6, 15, 12, 38, 2, 529, DateTimeKind.Local).AddTicks(2666), "Abdiel_Bahringer" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2019, 6, 15, 12, 38, 2, 560, DateTimeKind.Local).AddTicks(9729), "Kristofer.Miller@yahoo.com", "vRcYRAtMaqkkAib9KttNzAwg/VgYvC5L//Ea1rxSSoU=", "dNejYGxGcPF/GHddc69qRzzaVVcea4iKFOgI6u0ZUQs=", new DateTime(2019, 6, 15, 12, 38, 2, 560, DateTimeKind.Local).AddTicks(9778), "Lou_Stark98" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 579, DateTimeKind.Local).AddTicks(3898), "Filomena_OConnell11@gmail.com", "f+vqY3wf6qWkNkUWM1rKyGvTu38Pu7XStn9FanzeOqk=", "WB4k9QErnfh79rsCqBa58YPTorYtPvCah9eDgtNSz2M=", new DateTime(2019, 6, 15, 12, 38, 2, 579, DateTimeKind.Local).AddTicks(3947), "Grace_Cummings1" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2019, 6, 15, 12, 38, 2, 619, DateTimeKind.Local).AddTicks(6494), "Kiana.Barrows@hotmail.com", "kauMiFObpTNszTf6nKdKm6BjK38Dh0FuhVIaq+DjYlA=", "9XD7DdpY6zwW7Y4Htd2VcWcbU7AMa71DnvMAIL0pQec=", new DateTime(2019, 6, 15, 12, 38, 2, 619, DateTimeKind.Local).AddTicks(6543), "Golden.Crona" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2019, 6, 15, 12, 38, 2, 633, DateTimeKind.Local).AddTicks(120), "Jonas_Marquardt10@yahoo.com", "6Wrbz+duU6bxnTHhR9hBJUxh5JScGdA9IkWtuIs+kp8=", "z8FCGguvGesopRLiZFJlQXzCAxZAj+vXd1MHhflagXY=", new DateTime(2019, 6, 15, 12, 38, 2, 633, DateTimeKind.Local).AddTicks(169), "Junius81" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2019, 6, 15, 12, 38, 2, 662, DateTimeKind.Local).AddTicks(7628), "Kiarra_Haley@yahoo.com", "YFvCarX6Iwk7PJl1CLTHHuMa2Fuwun5oALSaS2LpyJQ=", "9S5aQzHtNfvM0JZjBeXPDMK6sqfC0sbL3Chk30qNdXU=", new DateTime(2019, 6, 15, 12, 38, 2, 662, DateTimeKind.Local).AddTicks(7673), "Jaquelin4" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2019, 6, 15, 12, 38, 2, 675, DateTimeKind.Local).AddTicks(3661), "Cornelius_Dickinson65@hotmail.com", "3qJn0Xm5cZpcRuuFgYJVF7JXfD/zcvh3S+C1o9eb9mU=", "X8yvK+LrEY/0UbxjoetQ3Rs0LVF+ThbPtuFLAw44clI=", new DateTime(2019, 6, 15, 12, 38, 2, 675, DateTimeKind.Local).AddTicks(3707), "Bret_Kohler11" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2019, 6, 15, 12, 38, 2, 689, DateTimeKind.Local).AddTicks(4016), "Graciela90@hotmail.com", "9uDV12Tosa7ZO8HZjxIlZEqNFXdyeN+1OtRQT1dC/BQ=", "66s7L+RQIAxm84J/iPMd/HNmDEXQaJiEhdHFTkyvKCY=", new DateTime(2019, 6, 15, 12, 38, 2, 689, DateTimeKind.Local).AddTicks(4057), "Cecile_Von99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2019, 6, 15, 12, 38, 2, 705, DateTimeKind.Local).AddTicks(1267), "Eugenia.Rodriguez@yahoo.com", "cXQZCqMCmEKBiyBiwjdwftdDmfYd64filW33M2tn3j8=", "MYL90OvE1WcSkonCGpPhPO4hHE39r2L2jQvhSr7g6EE=", new DateTime(2019, 6, 15, 12, 38, 2, 705, DateTimeKind.Local).AddTicks(1320), "Melvin3" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 719, DateTimeKind.Local).AddTicks(2086), "Davion36@yahoo.com", "igS3aafB/KITt3mk4bNybXABuNyEcuZnu0Sh4VpA5YI=", "af/M3UKBQK+0+NGyY/t4HCmjU5OeitGxJnIGAvblPeU=", new DateTime(2019, 6, 15, 12, 38, 2, 719, DateTimeKind.Local).AddTicks(2131), "Enola_Stroman33" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2019, 6, 15, 12, 38, 2, 733, DateTimeKind.Local).AddTicks(7538), "Heloise73@gmail.com", "c2FJUnxLnyt9Lmkj4PgJTgp2QhzdPchU4hrKN2qhRqM=", "qbqF3ojaMs39BIcv5yezPkgzWNoa9m3BeGaRJJOAoME=", new DateTime(2019, 6, 15, 12, 38, 2, 733, DateTimeKind.Local).AddTicks(7584), "Cordia_Casper7" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 15, 12, 38, 2, 774, DateTimeKind.Local).AddTicks(3128), "Elza_Moen17@hotmail.com", "fqNntOrV2gckC1sGY65syk4xKl6QxP6+ADb9kWCq1TY=", "/UeRM/YYWfTR6eb0C9Q6g/EatdbD1I1JYOHeEZW7J+g=", new DateTime(2019, 6, 15, 12, 38, 2, 774, DateTimeKind.Local).AddTicks(3173), "Juanita.Gaylord" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 15, 12, 38, 2, 786, DateTimeKind.Local).AddTicks(9240), "/0o9yntGq+1qsb7I7jU75vlU8m+8jViKWroQit2RDQ0=", "bXADMnkjNxTtA/+1dGmT2Pd5PA9VZedC6JBP8cqDFLU=", new DateTime(2019, 6, 15, 12, 38, 2, 786, DateTimeKind.Local).AddTicks(9240) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsHidden",
                table: "Posts");

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 13, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(4071), 0, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(5170), 16 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 17, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6321), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6336), 6 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6393), 0, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6397), 11 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 18, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6427), 0, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6431), 21 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 15, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6461), -1, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6465), 4 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6491), 1, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6495), 13 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 5, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6521), 0, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6529), 4 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 10, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6555), 1, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6559), 18 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 18, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6646), -1, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6650), 8 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 12, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6684), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6687), 12 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 5, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6725), -1, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6729), 7 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6763), -1, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6770), 10 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 10, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6797), -1, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6801), 8 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 20, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6838), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6842), 5 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 11, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6869), 0, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6872), 15 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 5, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6899), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6903), 11 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 17, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6933), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6937), 17 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { 13, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6963), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6967), 21 });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CommentId", "CreatedAt", "UpdatedAt" },
                values: new object[] { 19, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(6997), new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(7001) });

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CommentId", "CreatedAt", "LikeType", "UpdatedAt", "UserId" },
                values: new object[] { 19, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(7031), 0, new DateTime(2019, 6, 14, 20, 31, 38, 745, DateTimeKind.Local).AddTicks(7035), 1 });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Voluptatem nemo vitae explicabo labore tempora temporibus labore consequatur autem.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(1651), 3, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(2349) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Natus sed rem corporis.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3135), 8, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3176) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Officiis deserunt et ipsa culpa.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3252), 3, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3255) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Aliquid vel non quia eos accusantium.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3353), 8, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3357) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Quia totam totam provident quia quia.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3406), 12, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3410) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Et id ut.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3448), 20, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3452) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Nostrum et et tempore aut eaque sint.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3497), 3, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3501) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Necessitatibus doloribus ut nostrum minima.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3542), 19, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3546) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Beatae dolor aut rerum omnis unde ut veritatis atque.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3599), 12, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3603) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Non tenetur ea ut rerum itaque.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3690), 20, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3693) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Ex est id ex quia et sit.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3735), 6, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3742) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 9, "Dolorum illum voluptatem voluptatem autem illum voluptate ratione sint.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3791), new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3795) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Alias nostrum minus eum itaque officia commodi.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3841), 7, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3844) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Rerum amet quisquam sunt.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3882), 11, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3886) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Sed recusandae necessitatibus numquam qui tenetur natus.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3961), 13, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(3969) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Vitae deleniti temporibus dignissimos similique illum et.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4014), 7, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4018) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Ut dolores maxime consequatur et dignissimos qui.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4060), 20, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4067) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Eius et officiis unde in totam at.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4109), 6, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4112) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Quia qui ullam aut minima.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4150), 12, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4154) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Autem quis sint ut necessitatibus repudiandae aliquam dolorum consectetur.", new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4203), 8, new DateTime(2019, 6, 14, 20, 31, 38, 722, DateTimeKind.Local).AddTicks(4207) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 481, DateTimeKind.Local).AddTicks(109), "https://s3.amazonaws.com/uifaces/faces/twitter/moynihan/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 481, DateTimeKind.Local).AddTicks(8250) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 481, DateTimeKind.Local).AddTicks(9987), "https://s3.amazonaws.com/uifaces/faces/twitter/jarjan/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 481, DateTimeKind.Local).AddTicks(9995) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(33), "https://s3.amazonaws.com/uifaces/faces/twitter/carlyson/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(36) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(55), "https://s3.amazonaws.com/uifaces/faces/twitter/dreizle/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(59) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(78), "https://s3.amazonaws.com/uifaces/faces/twitter/rohixx/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(86) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(101), "https://s3.amazonaws.com/uifaces/faces/twitter/krasnoukhov/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(104) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(123), "https://s3.amazonaws.com/uifaces/faces/twitter/michzen/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(127) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(169), "https://s3.amazonaws.com/uifaces/faces/twitter/johnsmithagency/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(172) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(191), "https://s3.amazonaws.com/uifaces/faces/twitter/buryaknick/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(195) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(210), "https://s3.amazonaws.com/uifaces/faces/twitter/saschadroste/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(218) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(233), "https://s3.amazonaws.com/uifaces/faces/twitter/ryhanhassan/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(237) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(255), "https://s3.amazonaws.com/uifaces/faces/twitter/GavicoInd/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(259) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(274), "https://s3.amazonaws.com/uifaces/faces/twitter/davecraige/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(282) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(297), "https://s3.amazonaws.com/uifaces/faces/twitter/ramanathan_pdy/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(301) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(320), "https://s3.amazonaws.com/uifaces/faces/twitter/aoimedia/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(323) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(339), "https://s3.amazonaws.com/uifaces/faces/twitter/taylorling/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(346) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(361), "https://s3.amazonaws.com/uifaces/faces/twitter/posterjob/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(365) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(380), "https://s3.amazonaws.com/uifaces/faces/twitter/oskarlevinson/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(384) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(403), "https://s3.amazonaws.com/uifaces/faces/twitter/dpmachado/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(407) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(425), "https://s3.amazonaws.com/uifaces/faces/twitter/nandini_m/128.jpg", new DateTime(2019, 6, 14, 20, 31, 38, 482, DateTimeKind.Local).AddTicks(429) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(663), "https://picsum.photos/640/480/?image=1076", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(1739) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2053), "https://picsum.photos/640/480/?image=399", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2068) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2109), "https://picsum.photos/640/480/?image=132", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2117) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2211), "https://picsum.photos/640/480/?image=350", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2219) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2253), "https://picsum.photos/640/480/?image=492", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2260) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2294), "https://picsum.photos/640/480/?image=924", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2302) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2336), "https://picsum.photos/640/480/?image=1000", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2343) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2374), "https://picsum.photos/640/480/?image=452", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2381) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2415), "https://picsum.photos/640/480/?image=682", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2423) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2453), "https://picsum.photos/640/480/?image=232", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2460) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2494), "https://picsum.photos/640/480/?image=867", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2502) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2532), "https://picsum.photos/640/480/?image=853", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2540) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2574), "https://picsum.photos/640/480/?image=502", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2581) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2615), "https://picsum.photos/640/480/?image=19", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2623) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2657), "https://picsum.photos/640/480/?image=946", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2661) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2706), "https://picsum.photos/640/480/?image=928", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2713) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2747), "https://picsum.photos/640/480/?image=490", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2755) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2789), "https://picsum.photos/640/480/?image=135", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2796) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2827), "https://picsum.photos/640/480/?image=397", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2834) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2868), "https://picsum.photos/640/480/?image=898", new DateTime(2019, 6, 14, 20, 31, 38, 489, DateTimeKind.Local).AddTicks(2876) });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 735, DateTimeKind.Local).AddTicks(8837), 0, 14, new DateTime(2019, 6, 14, 20, 31, 38, 735, DateTimeKind.Local).AddTicks(9842), 12 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(865), 1, 8, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(876), 1 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(929), 1, 5, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(933), 13 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(963), 0, 20, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(967), 3 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(993), new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(997), 7 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1024), 4, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1027), 19 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1058), -1, 19, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1061), 12 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1092), -1, 10, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1095), 13 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1122), 1, 6, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1126), 15 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1152), -1, 6, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1156), 21 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1182), -1, 16, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1186), 6 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1212), -1, 16, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1216), 21 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1243), 1, 6, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1246), 1 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1273), 1, 7, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1277) });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1307), 6, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1311), 16 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1337), 13, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1341), 10 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1416), -1, 8, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1420), 10 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1450), 12, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1454), 9 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1484), 1, 19, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1488), 9 });

            migrationBuilder.UpdateData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "LikeType", "PostId", "UpdatedAt", "UserId" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1515), 0, 8, new DateTime(2019, 6, 14, 20, 31, 38, 736, DateTimeKind.Local).AddTicks(1518), 18 });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Neque rerum dolorum quo sit et aperiam vero quidem tempore.", new DateTime(2019, 6, 14, 20, 31, 38, 713, DateTimeKind.Local).AddTicks(2417), 33, new DateTime(2019, 6, 14, 20, 31, 38, 713, DateTimeKind.Local).AddTicks(3161) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Accusantium eos neque sit.", new DateTime(2019, 6, 14, 20, 31, 38, 713, DateTimeKind.Local).AddTicks(4580), 22, new DateTime(2019, 6, 14, 20, 31, 38, 713, DateTimeKind.Local).AddTicks(4599) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Natus quidem omnis sed.
Ex et aut accusantium numquam optio dolore.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(2426), 40, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(2494) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Vitae blanditiis sunt esse tenetur non iusto accusantium ut. Necessitatibus voluptas sunt corrupti et illo minus inventore architecto. Iure cupiditate quas sunt natus animi. Eligendi et quasi qui corporis iusto voluptatem placeat. Sit minus est.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(5595), 29, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(5610) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Quia ut recusandae qui ut consectetur modi.
Temporibus dolorem quis iste tenetur sint vero aliquid culpa quod.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(5750), 23, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(5753) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "officia", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6460), 27, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6467) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, @"Repellat qui et.
Debitis commodi asperiores.
Numquam nam et nihil qui pariatur.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6580), 37, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6588) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Adipisci dolores quas debitis aliquam nulla qui ea aut atque.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6648), 32, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6652) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "doloribus", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6694), 39, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6697) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "non", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6724), 23, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6728) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "A quia facilis est.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6769), 34, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6773) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Amet architecto inventore dolorem perspiciatis non.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6848), 31, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6852) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "aut", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6882), 31, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6886) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Aut adipisci quas repudiandae quas similique et.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6947), 27, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6950) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "qui", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6977), 40, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(6981) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Similique ipsum ut voluptatem.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7022), 30, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7026) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, @"Ducimus impedit fuga aut sit.
Error sit porro dignissimos ipsam.
Sit omnis vel consequatur et officiis.
Et quis perspiciatis eaque voluptatem rerum et quidem.
Blanditiis voluptatem numquam nostrum.
Ipsum laudantium autem rerum.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7173), 31, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7177) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "harum", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7207), 29, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7211) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Iure voluptate qui numquam expedita nihil qui explicabo sapiente.
Tempore voluptatum eius nihil eligendi quia error eligendi incidunt accusantium.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7302), 25, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7305) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Et minima eveniet.", new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7343), 33, new DateTime(2019, 6, 14, 20, 31, 38, 714, DateTimeKind.Local).AddTicks(7347) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2019, 6, 14, 20, 31, 38, 535, DateTimeKind.Local).AddTicks(3706), "Johanna14@hotmail.com", "0nFK/ulgZxcWwlNKkiGrpo9QTqjTTotI0U5APDM+PDg=", "Wzu324yb4hl3OzpLnJXiv8jqzG87Sp2hmNNjcFkd51w=", new DateTime(2019, 6, 14, 20, 31, 38, 535, DateTimeKind.Local).AddTicks(5076), "Joana.Corwin" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2019, 6, 14, 20, 31, 38, 544, DateTimeKind.Local).AddTicks(1815), "Ron_Wuckert94@yahoo.com", "+LM3ikTVyBPW+EQVXjmihO86jQgYmiohkX9sCvNeO3o=", "YU7004Cq31zvU+pQjaEf8cvXKEsCjDrSgUWPj07mC9k=", new DateTime(2019, 6, 14, 20, 31, 38, 544, DateTimeKind.Local).AddTicks(1849), "Jayne_Schroeder31" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2019, 6, 14, 20, 31, 38, 551, DateTimeKind.Local).AddTicks(7395), "Emilio.OReilly52@gmail.com", "0nO6zTJbq8hZFkY4RPKBf6fV2CsCSBHtEdeN+QyuOxs=", "jq61q6DecpXbrKArZlO1MbTXYBo3h4bITM5glGx6SJA=", new DateTime(2019, 6, 14, 20, 31, 38, 551, DateTimeKind.Local).AddTicks(7418), "Wendy87" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 14, 20, 31, 38, 560, DateTimeKind.Local).AddTicks(2268), "Esther27@gmail.com", "OBdmBEmUC21xRgfz9Ir/Y3/hTQFyNmYscFr+K+pWW40=", "/w7ZOulgFjkq5c4b2Ewhxi8ye6MvBXUiNcnQ7UPiz2g=", new DateTime(2019, 6, 14, 20, 31, 38, 560, DateTimeKind.Local).AddTicks(2302), "Shakira_Reichel0" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2019, 6, 14, 20, 31, 38, 567, DateTimeKind.Local).AddTicks(9721), "Erica_Stanton@gmail.com", "PkSDSZTK3xmjEKuiZdGXpgyZgVEkZUTIQtkqoKA+I1A=", "VCGoKAXcBCEf5BGEhBD75jHhUD2tDxZN7DsqOyPcqQs=", new DateTime(2019, 6, 14, 20, 31, 38, 567, DateTimeKind.Local).AddTicks(9789), "Dandre.Osinski85" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 14, 20, 31, 38, 576, DateTimeKind.Local).AddTicks(5228), "Darrin.King@hotmail.com", "YRmjFzb+AaX3THJPmBwfMB2BOnYS/Wjnm2uGa5fAQ+M=", "Jy9SwzslD+Ai7/r/TQ+rWcbbtkI+8nN5x2vfe76HPqc=", new DateTime(2019, 6, 14, 20, 31, 38, 576, DateTimeKind.Local).AddTicks(5262), "Wayne40" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2019, 6, 14, 20, 31, 38, 584, DateTimeKind.Local).AddTicks(2905), "Theresa95@yahoo.com", "NiutEov71hHNV+nWuA9vHEnKygZKQYiuR5fld5gAcko=", "srJXtpNurgedI/tM87ChP0EutrR9MaPo6LlSB+NvG4E=", new DateTime(2019, 6, 14, 20, 31, 38, 584, DateTimeKind.Local).AddTicks(2935), "Luisa_Block47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2019, 6, 14, 20, 31, 38, 592, DateTimeKind.Local).AddTicks(4277), "Karli88@hotmail.com", "XgaRQuX1dTWR6652EgPQK6wkxztbEyo3AYIfXtpptUk=", "LFbRfkEf1MqXtIwxsz0EAvC+ePrN6hn7CK5QuufBPRs=", new DateTime(2019, 6, 14, 20, 31, 38, 592, DateTimeKind.Local).AddTicks(4326), "Gust.Leannon89" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2019, 6, 14, 20, 31, 38, 600, DateTimeKind.Local).AddTicks(6140), "Stephanie_Kemmer22@hotmail.com", "mAwoCl2FZ2O4AaVnG8Q0+HjgWuVrUHA70MZpmtb/b/8=", "vj+5gGjGJBRo/tS4U6d8EW4Snh4+n8KRjR7ddNTBgYo=", new DateTime(2019, 6, 14, 20, 31, 38, 600, DateTimeKind.Local).AddTicks(6178), "Fiona16" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2019, 6, 14, 20, 31, 38, 609, DateTimeKind.Local).AddTicks(160), "Piper_Lemke@yahoo.com", "JYveRXU4gN3SmuL7+STQyE6kkx/aYJIsqVJyLuHh37I=", "zzcRWtF/Z1HvADuvCMpZUEzXKYWpbxgBGJmvEdj1NMw=", new DateTime(2019, 6, 14, 20, 31, 38, 609, DateTimeKind.Local).AddTicks(209), "Justyn86" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 617, DateTimeKind.Local).AddTicks(2242), "Marquise.Cruickshank@gmail.com", "NlGCtYMVjv8h8lJNhLBPTbtZdz4rNAp4skSAM9knxaU=", "+omX6wycUe4OigLG9Om9c++dDlfOIzV0eLY7vr7NpRI=", new DateTime(2019, 6, 14, 20, 31, 38, 617, DateTimeKind.Local).AddTicks(2280), "Lela_Huels54" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 14, 20, 31, 38, 625, DateTimeKind.Local).AddTicks(3335), "Willow_Kilback4@yahoo.com", "lWKfFAHq89uy3DNd2KlQkTNNKzjgfvRfVNlgdamlNVs=", "AcsbGKmXnJEKNc3+5HDx8HRh4rkLdcITxvvDbQriUvU=", new DateTime(2019, 6, 14, 20, 31, 38, 625, DateTimeKind.Local).AddTicks(3373), "Sadye.Murazik" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2019, 6, 14, 20, 31, 38, 633, DateTimeKind.Local).AddTicks(739), "Carolyn.Franecki71@yahoo.com", "E2ZLwUVJnh9YMnYpvr9tEW7Udc1/x0BjjXhP6vsrNpc=", "2NfFuYNKzuHT5Bn/ZfvFEjhnnDQ4zeqZ17TY35ILN8c=", new DateTime(2019, 6, 14, 20, 31, 38, 633, DateTimeKind.Local).AddTicks(773), "Joshuah_Deckow" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2019, 6, 14, 20, 31, 38, 640, DateTimeKind.Local).AddTicks(8767), "Orland_Gusikowski55@yahoo.com", "9ibj/hSmlVrzaB66wrS4v/S2G4PrzPUeKCzDKnkGnFw=", "/8RyDXiUpvSj8clRo3iy/R3PKG0qs8ELQHygw6Bxh8w=", new DateTime(2019, 6, 14, 20, 31, 38, 640, DateTimeKind.Local).AddTicks(8800), "Nels18" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2019, 6, 14, 20, 31, 38, 649, DateTimeKind.Local).AddTicks(3590), "Celine41@yahoo.com", "J8qNE/AXwLVDB7PQfd3kmpBrSeKlAH62gsvBkcKRTlg=", "H5AHKTFoXfy+MIRrYtzpAQ51YYRylfDiwrm0cJ2CraQ=", new DateTime(2019, 6, 14, 20, 31, 38, 649, DateTimeKind.Local).AddTicks(3624), "Leonel_Glover" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2019, 6, 14, 20, 31, 38, 656, DateTimeKind.Local).AddTicks(9707), "Fabiola41@yahoo.com", "t18bsqutUQZPMC/s2CVaWUnub9+qTbNAiqhsRheOO0s=", "WnFvzYbPFPcKV41Nk9630osYA0VPzxxltQhPxr+xV6Q=", new DateTime(2019, 6, 14, 20, 31, 38, 656, DateTimeKind.Local).AddTicks(9892), "Tom46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2019, 6, 14, 20, 31, 38, 667, DateTimeKind.Local).AddTicks(3916), "Adrianna.Green@gmail.com", "ac9Puj0qCb3N0fHqmdb0H551dxrvEjzhLS+J0/rbsJs=", "DMlGoIgn1hoIchEA1jAdbDh/Q9hVWlRnWlcLP78QJKc=", new DateTime(2019, 6, 14, 20, 31, 38, 667, DateTimeKind.Local).AddTicks(3950), "Shirley91" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 676, DateTimeKind.Local).AddTicks(7231), "Jerome.Jenkins98@hotmail.com", "ptRC0Zk3NvFri14vOme2OOqbiqEBxsodzuEoG1ZZgx8=", "hMuURpNsu08ExiZ3MiYdugaLtBfviBEVYQplYZd6f3E=", new DateTime(2019, 6, 14, 20, 31, 38, 676, DateTimeKind.Local).AddTicks(7273), "Eleanore.Veum" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2019, 6, 14, 20, 31, 38, 684, DateTimeKind.Local).AddTicks(5126), "Oliver_OConner75@yahoo.com", "uJsP7ziCHg0+8yNSm91iGzY1kfftcsVnb6UT9GE796k=", "4IIt8aQ9VhRP+cfNryt1wF9q20LiGklxf0/7h48UCj0=", new DateTime(2019, 6, 14, 20, 31, 38, 684, DateTimeKind.Local).AddTicks(5160), "Rey75" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 14, 20, 31, 38, 692, DateTimeKind.Local).AddTicks(9334), "Anissa.Carter53@hotmail.com", "3b14K7H7kBmmimDRl5V9NWQQ1f1ODDkmszNTlaF0UMs=", "k47qFLVY5tyqnmygx5BufG86gVMG7yArQjS55qaqW2I=", new DateTime(2019, 6, 14, 20, 31, 38, 692, DateTimeKind.Local).AddTicks(9372), "Bridgette_Powlowski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 14, 20, 31, 38, 700, DateTimeKind.Local).AddTicks(8098), "hKqfEGGijPt2ZR/7y4fJJbd66q6NDcTRTYjsicH9Wqc=", "QlcBKr5wMuc2pIMQcVj3GTLcjgma7oDH9zddQKDZEtk=", new DateTime(2019, 6, 14, 20, 31, 38, 700, DateTimeKind.Local).AddTicks(8098) });
        }
    }
}
